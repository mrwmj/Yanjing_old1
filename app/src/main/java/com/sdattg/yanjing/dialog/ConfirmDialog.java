package com.sdattg.yanjing.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.sdattg.yanjing.R;

/**
 * Created by Administrator on 2019/7/8.
 */

@SuppressLint("ValidFragment")
public class ConfirmDialog extends DialogFragment {

    /**
     * 日志打印
     */
    private static final String TAG = "ConfirmDialog";
    private Context mContent;
    private TextView tvTitle;
    private TextView tvContent;
    private TextView tvCancel;
    private TextView tvSubmit;
    /**
     * 取消点击
     */
    private View.OnClickListener mCancleClick;
    /**
     * 确认点击
     */
    private View.OnClickListener mConfirmClick;
    @SuppressLint("ValidFragment")
    public ConfirmDialog(Context mContent) {
        this.mContent = mContent;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_confirm, container, false);

        tvTitle = view.findViewById(R.id.xt_tv_title);
        tvContent = view.findViewById(R.id.xt_tv_content);
        tvCancel = view.findViewById(R.id.xt_tv_confirm_cancel);
        tvSubmit = view.findViewById(R.id.xt_tv_confirm_submit);
        Bundle bundle = getArguments();
        if (bundle != null) {
            String title= bundle.getString("title");
            String content= bundle.getString("content");
            tvTitle.setText(title);
            tvContent.setText(content);
        }

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mConfirmClick!=null){
                    mConfirmClick.onClick(v);
                }
            }
        });
        //取消
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCancleClick!=null){
                    mCancleClick.onClick(v);
                }
            }
        });

        setCancelable(false);//设置外部点击不可取消
        return view;
    }
    public void setmOnCancleListener(View.OnClickListener mCancleClick){
        this.mCancleClick = mCancleClick;
    }
    public void setmOnConfirmListener(View.OnClickListener mConfirmClick){
        this.mConfirmClick = mConfirmClick;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 设置对话框的样式
        setStyle(STYLE_NO_FRAME,R.style.TranslucentNoTitle);
    }

    @Override
    public void onStart() {
        // 1, 设置对话框的大小
        Window window = getDialog().getWindow();
        WindowManager wm = window.getWindowManager();
        Point windowSize = new Point();
        wm.getDefaultDisplay().getSize(windowSize);
        float size_x = 0;
        float size_y = 0;
        int width = windowSize.x;
        int height = windowSize.y;
        if (width >= height) {// 横屏
            size_x = 0.7f;
            size_y = 0.5f;
            window.getAttributes().width = (int) (windowSize.y * size_x * 1.1);
            window.getAttributes().height = (int) (windowSize.y * size_y);
        } else {// 竖屏
            size_x = 0.7f;
            size_y = 0.5f;
            window.getAttributes().width = (int) (windowSize.x * size_x);
            window.getAttributes().height = (int) (windowSize.x * size_y);
        }
        window.setGravity(Gravity.CENTER);
        super.onStart();
    }

    public static class Builder {
        /**
         * 存放数据的容器
         **/
        private Bundle mmBundle;
        private View.OnClickListener mmCancleClick;
        private View.OnClickListener mmConfirmClick;
        public Builder() {
            mmBundle = new Bundle();
        }
        public Builder setTtile(CharSequence title) {
            mmBundle.putCharSequence("title", title);
            return this;
        }

        public Builder setContent(CharSequence content) {
            mmBundle.putCharSequence("content", content);
            return this;
        }
        public Builder setOnCancleListener(View.OnClickListener mmCancleClick){
            this.mmCancleClick = mmCancleClick;
            return this;
        }
        public Builder setOnConfirmListener(View.OnClickListener mmConfirmClick){
            this.mmConfirmClick = mmConfirmClick;
            return this;
        }

        private ConfirmDialog create(Context context) {
            final ConfirmDialog dialog = new ConfirmDialog(context);
            dialog.setArguments(mmBundle);
            dialog.setmOnConfirmListener(mmConfirmClick);
            dialog.setmOnCancleListener(mmCancleClick);
            return dialog;
        }
        public ConfirmDialog show(Context context, FragmentManager fm) {
            if (fm == null) {
                Log.e(TAG, "show error : fragment manager is null.");
                return null;
            }
            ConfirmDialog dialog = create(context);
            Log.d(TAG, "show ConfirmDialog");
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(dialog, TAG);
            ft.show(dialog);
            ft.commitAllowingStateLoss();
            return dialog;
        }
    }


}
