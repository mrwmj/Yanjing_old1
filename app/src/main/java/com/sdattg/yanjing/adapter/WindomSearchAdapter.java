package com.sdattg.yanjing.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.bean.SearchBen;
import com.sdattg.yanjing.config.ConfigKey;
import com.sdattg.yanjing.config.ConfigUtils;
import com.sdattg.yanjing.interfaces.OnSearchItemClickLister;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2019/8/9.
 */

public class WindomSearchAdapter extends RecyclerView.Adapter<WindomSearchAdapter.SearchHolder> {

    private ArrayList<SearchBen> list = new ArrayList<>();
    private Context mContext;
    private OnSearchItemClickLister itemClickLister;
    private ConfigUtils mConfigUtils;

    public WindomSearchAdapter(ArrayList<SearchBen> list, Context mContext, OnSearchItemClickLister itemClickLister) {
        this.list.clear();
        this.list.addAll(list);
        this.mContext = mContext;
        this.itemClickLister = itemClickLister;
        mConfigUtils = new ConfigUtils(mContext);
    }

    public void setList(ArrayList<SearchBen> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SearchHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(mContext).inflate(R.layout.adapter_windom_search, parent, false);
        SearchHolder searchHolder = new SearchHolder(inflate);
        return searchHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchHolder holder, int position) {
        final SearchBen searchBen = list.get(position);
        int search_mode = mConfigUtils.getInt(ConfigKey.SEARCH_MODE, 4);
        if (searchBen.getId() == search_mode) {
            holder.ivClose.setVisibility(View.VISIBLE);
        } else {
            holder.ivClose.setVisibility(View.INVISIBLE);
        }
        holder.tvLoginid.setText(searchBen.getName());
        Log.i("WindomSearchAdapter", "onBindViewHolder: " + searchBen.getName());
        holder.rootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mConfigUtils.put(ConfigKey.SEARCH_MODE, searchBen.getId());
                itemClickLister.onItemClick(searchBen);
            }
        });

    }

    @Override

    public int getItemCount() {
        return list.size();
    }

    public class SearchHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_loginid)
        TextView tvLoginid;
        @Bind(R.id.iv_close)
        ImageView ivClose;
        @Bind(R.id.root_layout)
        LinearLayout rootLayout;

        public SearchHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
