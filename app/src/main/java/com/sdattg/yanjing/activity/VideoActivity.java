package com.sdattg.yanjing.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.adapter.VideoAdapter;
import com.sdattg.yanjing.base.BaseActivity;
import com.sdattg.yanjing.bean.VideoBean;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 视频
 * Created by Administrator on 2019/7/24.
 */

public class VideoActivity extends BaseActivity {
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    private ArrayList<VideoBean> list = new ArrayList<>();
    private VideoAdapter videoAdapter;

    @Override
    public int getLayout() {
        return R.layout.activity_video;
    }

    @Override
    public void initView() {
        whiteBack.setVisibility(View.GONE);
        setMiddleTitle("视频");
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        for (int i = 0; i < 50; i++) {
            VideoBean videoBean = new VideoBean();
            videoBean.setUrl(R.drawable.bg_video);
            videoBean.setName("创世纪");
            videoBean.setInfo(" 创世记(Genesis)这个字意思即“始原”，“起源”。\n" +
                    "    创世记书中所记的，事实上也是有关各种事物的开始。\n" +
                    "    因此，我们给这本书的题目是：“开始”");
            videoBean.setUrl(R.drawable.picture);
            list.add(videoBean);
        }
        videoAdapter = new VideoAdapter(mContext);
        recyclerView.setAdapter(videoAdapter);
        videoAdapter.setVideo(list);
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {

    }


}
