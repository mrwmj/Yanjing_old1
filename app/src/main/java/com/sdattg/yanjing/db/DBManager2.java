package com.sdattg.yanjing.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Administrator on 2017/9/29.
 */

public class DBManager2 {
    private String DB_NAME = "yanjing.db";
    private Context mContext;

    public DBManager2(Context mContext) {
        this.mContext = mContext;
    }

    //把assets目录下的db文件复制到dbpath下
    public SQLiteDatabase DBManager() {
        String dbPath = "/data/data/com.sdattg.yanjing/databases/" + DB_NAME;
        File file = new File("/data/data/com.sdattg.yanjing/databases");
        if (!file.exists()){
            try {
                FileOutputStream out = new FileOutputStream(dbPath);
                InputStream in = mContext.getAssets().open("yanjing.db");
                byte[] buffer = new byte[1024];
                int readBytes = 0;
                while ((readBytes = in.read(buffer)) != -1)
                    out.write(buffer, 0, readBytes);
                in.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return SQLiteDatabase.openOrCreateDatabase(dbPath, null);
    }

    //查询
    public Cursor query(SQLiteDatabase sqliteDB, String table, String selection, String[] selectionArgs, String orderBy) {
//        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = sqliteDB.query(table, null, selection, selectionArgs, null, null, orderBy);
        return cursor;
    }
}
