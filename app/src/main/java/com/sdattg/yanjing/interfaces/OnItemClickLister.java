package com.sdattg.yanjing.interfaces;

import android.widget.ImageView;

/**
 * Created by Administrator on 2018/1/19.
 */

public interface OnItemClickLister {
    void onItemClick(ImageView imageView, String path, int position);
}
