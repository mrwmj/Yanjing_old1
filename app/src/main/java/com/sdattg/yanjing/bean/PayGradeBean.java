package com.sdattg.yanjing.bean;

/**
 * Created by Administrator on 2019/7/28.
 */

public class PayGradeBean {
    private int id;
    private String price;
    private String days;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    @Override
    public String toString() {
        return "PayGradeAdapter{" +
                "id=" + id +
                ", price='" + price + '\'' +
                ", days='" + days + '\'' +
                '}';
    }
}
