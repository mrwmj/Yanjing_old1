package com.sdattg.yanjing.fragment.me;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.adapter.PayGradeAdapter;
import com.sdattg.yanjing.base.BaseFragment;
import com.sdattg.yanjing.bean.PayGradeBean;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2019/7/28.
 */

public class PayFragment extends BaseFragment {
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    private ArrayList<PayGradeBean> payGradeBeans = new ArrayList<>();
    private PayGradeAdapter payGradeAdapter;

    @Override
    public int getLayout() {
        return R.layout.fragment_pay;
    }

    @Override
    public void initView() {
        recyclerView.setLayoutManager(new GridLayoutManager(getContext().getApplicationContext(), 3));

        payGradeAdapter = new PayGradeAdapter(getContext());
        recyclerView.setAdapter(payGradeAdapter);
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {
        payGradeBeans.clear();
        PayGradeBean gradeBean = new PayGradeBean();
        gradeBean.setId(1);
        gradeBean.setDays(30 + "天");
        gradeBean.setPrice(15 + "元");
        payGradeBeans.add(gradeBean);


        PayGradeBean gradeBean2 = new PayGradeBean();
        gradeBean2.setId(2);
        gradeBean2.setDays(90 + "天");
        gradeBean2.setPrice(30 + "元");
        payGradeBeans.add(gradeBean2);

        PayGradeBean gradeBean3 = new PayGradeBean();
        gradeBean3.setId(3);
        gradeBean3.setDays(180 + "天");
        gradeBean3.setPrice(60 + "元");
        payGradeBeans.add(gradeBean3);

        PayGradeBean gradeBean4 = new PayGradeBean();
        gradeBean4.setId(4);
        gradeBean4.setDays(360 + "天");
        gradeBean4.setPrice(80 + "元");
        payGradeBeans.add(gradeBean4);

        PayGradeBean gradeBean5 = new PayGradeBean();
        gradeBean5.setId(5);
        gradeBean5.setDays("永久");
        gradeBean5.setPrice(88 + "元");
        payGradeBeans.add(gradeBean5);
        payGradeAdapter.setBooks(payGradeBeans);


    }
}
