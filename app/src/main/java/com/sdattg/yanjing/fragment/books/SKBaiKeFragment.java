package com.sdattg.yanjing.fragment.books;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.adapter.BooksAdapter;
import com.sdattg.yanjing.base.BaseFragment;
import com.sdattg.yanjing.bean.BooksBean;
import com.sdattg.yanjing.db.DBManager;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2019/7/24.
 */

@SuppressLint("ValidFragment")
public class SKBaiKeFragment extends BaseFragment {
    private static String TAG = "SKBaiKeFragment";
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    private Context mContext;
    private ArrayList<BooksBean> beans = new ArrayList<>();
    private BooksAdapter booksAdapter;
    private String type = "0";

    @SuppressLint("ValidFragment")
    public SKBaiKeFragment(String type) {
        this.type = type;
    }


    @Override
    public int getLayout() {
        mContext = getContext();
        return R.layout.fragment_sk_bai_ke;
    }

    @Override
    public void initView() {

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext().getApplicationContext()));

        booksAdapter = new BooksAdapter(mContext);
        recyclerView.setAdapter(booksAdapter);
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {
        Cursor cursor = dbManager.queryAll("books", "catalog_id=?", new String[]{type}, "books_min_name asc");
        while (cursor.moveToNext()) {
            String books_name = cursor.getString(cursor.getColumnIndex("books_name"));
            String books_min_name = cursor.getString(cursor.getColumnIndex("books_min_name"));
            String books_version = cursor.getString(cursor.getColumnIndex("books_version"));
            String books_summary = cursor.getString(cursor.getColumnIndex("books_summary"));
            String books_id = cursor.getString(cursor.getColumnIndex("books_id"));
            String catalog_rank = cursor.getString(cursor.getColumnIndex("catalog_rank"));
            String catalog_id = cursor.getString(cursor.getColumnIndex("catalog_id"));

            BooksBean booksBean = new BooksBean();
            booksBean.setBooks_name(books_name);
            booksBean.setBooks_min_name(books_min_name);
            booksBean.setBooks_version(books_version);
            booksBean.setBooks_summary(books_summary);
            booksBean.setBooks_id(books_id);
            booksBean.setCatalog_rank(catalog_rank);
            booksBean.setCatalog_id(catalog_id);
            beans.add(booksBean);
        }
        cursor.close();
        booksAdapter.setBooks(beans);
    }

}
