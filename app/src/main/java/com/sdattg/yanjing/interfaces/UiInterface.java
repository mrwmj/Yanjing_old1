package com.sdattg.yanjing.interfaces;

/**
 * ui界面的接口
 */
public interface UiInterface {
    /**
     * 获取布局的id
     */
    int getLayout();
    /**
     * 初始化控件
     */
    void initView();
    /**
     * 初始化监听
     */
    void initListener();
    /**
     * 初始化数据
     */
    void initData();
}
