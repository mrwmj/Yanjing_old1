package com.sdattg.yanjing.interfaces;

/**
 * Created by Administrator on 2018/1/3.
 */

public interface IOnFocusListener {
    void onWindowFocusChanged(boolean hasFocus);
}
