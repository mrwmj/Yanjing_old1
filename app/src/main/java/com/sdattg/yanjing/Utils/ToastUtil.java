package com.sdattg.yanjing.Utils;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

public class ToastUtil {
	   private static String oldMsg;
	    protected static Toast toast = null;
	    private static long oldTime = 0;
	    private static long currentTime = 0;
	      
	    private static void showToast(Context context, String s){
	        if(toast == null){
	            toast = Toast.makeText(context.getApplicationContext(), s, Toast.LENGTH_SHORT);
	            toast.show();  
	            oldTime = System.currentTimeMillis();
	        }else{  
	            currentTime = System.currentTimeMillis();
	            if(s.equals(oldMsg)){  
	                if((currentTime - oldTime) > Toast.LENGTH_SHORT){
	                    toast.show();  
	                }
	            }else{  
	                oldMsg = s;  
	                toast.setText(s);  
	                toast.show();  
	            }         
	        }  
	        oldTime = currentTime;
	    }  
	      
	    public static void show(Context context, String msg){
	    	showToast(context, msg);
	    }
	    public static void showToast(Context context, int resId){
	        showToast(context, context.getString(resId));  
	    }  
	    
	    public static void showToastInThread(final Activity context, final String msg){
			context.runOnUiThread(new Runnable(){
				@Override
				public void run() {
					Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
				}
			});
		}
}
