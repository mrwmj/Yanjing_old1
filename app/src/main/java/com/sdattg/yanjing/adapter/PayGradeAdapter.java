package com.sdattg.yanjing.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.bean.BooksBean;
import com.sdattg.yanjing.bean.PayGradeBean;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2019/7/26.
 */

public class PayGradeAdapter extends RecyclerView.Adapter<PayGradeAdapter.PayGradeHolder> {
    private static String TAG = "PayGradeAdapter";
    private ArrayList<PayGradeBean> books = new ArrayList<>();
    private Context mContext;

    public PayGradeAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setBooks(ArrayList<PayGradeBean> books) {
        this.books.clear();
        this.books.addAll(books);
        notifyDataSetChanged();
    }

    public void addBooks(ArrayList<PayGradeBean> books) {
        this.books.addAll(books);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PayGradeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(mContext).inflate(R.layout.adapter_pay_grade_item, parent, false);
        PayGradeHolder booksHolder = new PayGradeHolder(inflate);
        return booksHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PayGradeHolder holder, int position) {
        PayGradeBean booksBean = books.get(position);
        Log.i(TAG, "onBindViewHolder: " + booksBean.getDays());
        holder.tvPrice.setText(booksBean.getDays());

    }

    @Override
    public int getItemCount() {
        return books.size();
    }


    public class PayGradeHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_price)
        TextView tvPrice;

        public PayGradeHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

