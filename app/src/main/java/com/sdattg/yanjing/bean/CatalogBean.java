package com.sdattg.yanjing.bean;

/**
 * Created by Administrator on 2019/7/25.
 */

public class CatalogBean {
    private String catalog_name;//目录名称
    private String catalog_id;//目录id
    private String catalog_rank;//目录层级
    private String last_catalog_id;//上级目录id

    public String getCatalog_name() {
        return catalog_name;
    }

    public void setCatalog_name(String catalog_name) {
        this.catalog_name = catalog_name;
    }

    public String getCatalog_id() {
        return catalog_id;
    }

    public void setCatalog_id(String catalog_id) {
        this.catalog_id = catalog_id;
    }

    public String getCatalog_rank() {
        return catalog_rank;
    }

    public void setCatalog_rank(String catalog_rank) {
        this.catalog_rank = catalog_rank;
    }

    public String getLast_catalog_id() {
        return last_catalog_id;
    }

    public void setLast_catalog_id(String last_catalog_id) {
        this.last_catalog_id = last_catalog_id;
    }

    @Override
    public String toString() {
        return "CatalogBean{" +
                "catalog_name='" + catalog_name + '\'' +
                ", catalog_id='" + catalog_id + '\'' +
                ", catalog_rank='" + catalog_rank + '\'' +
                ", last_catalog_id='" + last_catalog_id + '\'' +
                '}';
    }
}
