package com.sdattg.yanjing.activity;

import android.app.LocalActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.sdattg.yanjing.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by 10196 on 2019/6/9.
 */
public class MainActivity extends AppCompatActivity {
    private static String TAG = "MainActivity";
    @Bind(R.id.frame_layout)
    FrameLayout frameLayout;
    @Bind(R.id.tab_book)
    RadioButton tabBook;
    @Bind(R.id.tab_audio)
    RadioButton tabAudio;
    @Bind(R.id.tab_video)
    RadioButton tabVideo;
    @Bind(R.id.tab_me)
    RadioButton tabMe;
    @Bind(R.id.tabs_rg)
    RadioGroup tabsRg;

    private Context mContext;

    final LocalActivityManager localActivityManager = new LocalActivityManager(this, true);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mContext = this;
        localActivityManager.dispatchCreate(savedInstanceState);

        initView();
        initListener();
        initData();

    }

    private void initView() {
        Intent intent = new Intent(MainActivity.this, BookActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        frameLayout.addView(localActivityManager.startActivity("Module1", intent).getDecorView());
    }

    private void initListener() {
        tabsRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.tab_book:
                        frameLayout.removeAllViews();
                        Intent intent = new Intent(MainActivity.this, BookActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        frameLayout.addView(localActivityManager.startActivity("Module1", intent).getDecorView());
                        break;
                    case R.id.tab_audio:
                        frameLayout.removeAllViews();
                        Intent intent2 = new Intent(MainActivity.this, AudioActivity.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        frameLayout.addView(localActivityManager.startActivity("Module2", intent2).getDecorView());
                        break;
                    case R.id.tab_video:
                        frameLayout.removeAllViews();
                        Intent intent3 = new Intent(MainActivity.this, VideoActivity.class);
                        intent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        frameLayout.addView(localActivityManager.startActivity("Module3", intent3).getDecorView());
                        break;
                    case R.id.tab_me:
                        frameLayout.removeAllViews();
                        Intent intent4 = new Intent(MainActivity.this, MeActivity.class);
                        intent4.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        frameLayout.addView(localActivityManager.startActivity("Module4", intent4).getDecorView());
                        break;
                }
            }
        });

    }

    private void initData() {
    }
}

