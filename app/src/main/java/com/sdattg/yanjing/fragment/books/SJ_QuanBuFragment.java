package com.sdattg.yanjing.fragment.books;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.base.BaseFragment;

/**
 * 圣经全部
 * Created by Administrator on 2019/8/12.
 */

public class SJ_QuanBuFragment extends BaseFragment{
    @Override
    public int getLayout() {
        return R.layout.fragment_sj_xin_yue;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {

    }
}
