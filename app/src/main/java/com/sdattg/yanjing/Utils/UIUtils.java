package com.sdattg.yanjing.Utils;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.sdattg.yanjing.base.BaseApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class UIUtils {

    private static Toast mToast;

    public static Context getContext() {
        return BaseApplication.getContext();
    }


    /**
     * 获取字符串
     */
    public static String getString(int id) {
        return getContext().getResources().getString(id);
    }

    /**
     * 获取图片
     */
    public static Drawable getDrawable(int id) {
        return getContext().getResources().getDrawable(id);
    }

    /**
     * 获取颜色
     */
    public static int getColor(int id) {
        return getContext().getResources().getColor(id);
    }

    /**
     * 获取颜色的状态选择器
     */
    public static ColorStateList getColorStateList(int id) {
        return getContext().getResources().getColorStateList(id);
    }

    /**
     * 获取尺寸,返回像素
     */
    public static int getDimen(int id) {
        return getContext().getResources().getDimensionPixelSize(id);
    }

    /**
     * 获取字符串数组
     */
    public static String[] getStringArray(int id) {
        return getContext().getResources().getStringArray(id);
    }

    /**
     * dip转px
     *
     * @param dp
     * @return
     */
    public static int dip2px(float dp) {
        return (int) (getContext().getResources().getDisplayMetrics().density
                * dp + 0.5f);
    }

    /**
     * px转dip
     *
     * @param px
     * @return
     */
    public static float px2dip(int px) {
        return px / getContext().getResources().getDisplayMetrics().density;
    }

    /**
     * 根据布局文件id加载布局
     *
     * @param layoutID
     * @return
     */
    public static View inflate(int layoutID) {
        return View.inflate(getContext(), layoutID, null);
    }

    /**
     * 判断是否在主线程运行
     *
     * @return
     */
//    public static boolean isRunOnUiThread() {
//        // 获取当前线程id, 和主线程id比对, 如果一致,就是主线程
//        return android.os.Process.myTid() == getMainThreadId();
//    }

    /**
     * 显示单例toast
     *
     * @param msg
     */
    public static void showToast(String msg) {
        if (mToast == null) {
            mToast = Toast.makeText(UIUtils.getContext(), "", Toast.LENGTH_SHORT);
        }
        mToast.setText(msg);
        mToast.show();
    }

    public static void cancelToast() {
        if (mToast != null) {
            mToast.cancel();
        }
    }

    /**
     * 校验手机号
     *
     * @param content
     * @return
     */
    public static boolean checkMobile(String content) {
        String phoneReg = "^1[034578][0-9]{9}$";
        if (!TextUtils.isEmpty(content) && content.matches(phoneReg)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 校验密码
     *
     * @param content
     * @return
     */
    public static boolean checkPwd(String content) {
        String pwdReg = "^[0-9a-zA-Z]*$";
        if (TextUtils.isEmpty(content) || content.length() < 6
                || content.length() > 20 || !content.matches(pwdReg)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 匹配6~20位由数字和字母混合而成的密码
     */
    public static boolean checkPwd2(String pwd) {
        String regex = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$";
        if (TextUtils.isEmpty(pwd) || !pwd.matches(regex)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 加载web网页标签
     *
     * @param web
     * @param bodyHtml
     */
    public static void setWebView(WebView web, String bodyHtml) {
        String head = "<head><style>img{max-width: 100%; width:auto; height: auto;}table{border-collapse:collapse!important;width: 100%!important;max-width: 720px!important;margin: 0 auto!important; font-size: 14px!important;color:#333!important;}</style></head>";
        String html = "<html>" + head + "<body>" + bodyHtml + "</body></html>";
        WebSettings ws = web.getSettings();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            ws.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
        } else {
            ws.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        }
        ws.setJavaScriptEnabled(false);
        web.loadDataWithBaseURL(null, html, "text/html", "utf-8", null);
    }

    /**
     * 对手机号中间四位加密
     *
     * @param oldMobile
     * @return
     */
    public static String encryptMobile(String oldMobile) {
        if (TextUtils.isEmpty(oldMobile)) {
            return "";
        }
        if (oldMobile.length() != 11) {
            return "";
        }
        String newMobile = oldMobile.substring(0, oldMobile.length() - (oldMobile.substring(3)).length()) + "****" + oldMobile.substring(7);
        return newMobile;
    }

    /**
     * 设置中划线
     *
     * @param text
     */
    public static void addMiddleLine(TextView text) {
        text.getPaint().setAntiAlias(true);//抗锯齿
        text.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);// 设置中划线并加清晰
    }

    /**
     * 取小数点后一位
     *
     * @param d
     * @return
     */
    public static String decimalFormat(double d) {
        java.text.DecimalFormat df = new java.text.DecimalFormat("#0.#");
        return df.format(d);
    }

    /**
     * 拨打电话
     *
     * @param context
     */
    public static void call(Context context, String tel) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + tel));
        context.startActivity(intent);
    }

    /**
     * 将字节(b)转换成(MB)
     */
    @NonNull
    public static String byteToMB(long b) {
        if (b > 0) {
            double kb = b / 1024;
            double mb = kb / 1024;
            String s = decimalFormat(mb);
            return s + "MB";
        } else {
            return "0MB";
        }
    }

    /**
     * 将字节(b)转换成(KB/MB)
     */
    @NonNull
    public static String byteToMB2(long b) {
        if (b > 0) {
            if (b > 1024) {
                double kb = b / 1024;
                if (kb > 1024) {
                    double mb = kb / 1024;
                    String s = decimalFormat(mb);
                    return s + "MB";
                } else {
                    return decimalFormat(kb) + "KB";
                }
            } else {
                return b + "B";
            }
        } else {
            return "0B";
        }
    }

    /**
     * 获得六位随机数
     *
     * @return
     */
    public static String getNumber() {
        Random random = new Random();
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < 6; i++) {
            buffer.append(random.nextInt(10));
        }
        return buffer.toString();
    }

    /**
     * 将时间戳转换为时间
     *
     * @return 输出格式为：yyyy-MM-dd
     */
    public static String getDateStr(long timestamp) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd EEEE");
        return sf.format(new Date(timestamp));
    }

    /**
     * 将时间戳转换为时间
     *
     * @return 输出格式为：yyyy-MM-dd
     */
    public static String getDateStr2(long timestamp) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return sf.format(new Date(timestamp));
    }

    /**
     * 判断一年的第几周
     *
     * @param timestamp
     * @return
     * @throws java.text.ParseException
     */
    public static int whatWeek(long timestamp) {
        Date date = new Date(timestamp);
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTime(date);
        int weekNumbe = calendar.get(Calendar.WEEK_OF_YEAR);
        return weekNumbe;
    }

    /**
     *加载图片
     */


    /**
     * 获取短信验证码
     */
//    public static String sendSms(String num) throws ClientException {
//        //设置超时时间-可自行调整
//        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
//        System.setProperty("sun.net.client.defaultReadTimeout", "10000");
//        //初始化ascClient需要的几个参数
//        final String product = "Dysmsapi";//短信API产品名称（短信产品名固定，无需修改）
//        final String domain = "dysmsapi.aliyuncs.com";//短信API产品域名（接口地址固定，无需修改）
//        //替换成你的AK
//        final String accessKeyId = "LTAIr93w5RoA3J5j";//你的accessKeyId,参考本文档步骤2
//        final String accessKeySecret = "EsHIl6I47r7jCiiWg1WXbtgOqxCAP9";//你的accessKeySecret，参考本文档步骤2
//        //初始化ascClient,暂时不支持多region（请勿修改）
//        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId,
//                accessKeySecret);
//        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
//        IAcsClient acsClient = new DefaultAcsClient(profile);
//        //组装请求对象
//        SendSmsRequest request = new SendSmsRequest();
//        //使用post提交
//        request.setMethod(MethodType.POST);
////        String num = etUsername.getText().toString();
//        //必填:待发送手机号。支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式
//        request.setPhoneNumbers(num);
//        //必填:短信签名-可在短信控制台中找到
//        request.setSignName("智慧健康服务云平台");
//        //必填:短信模板-可在短信控制台中找到
//        request.setTemplateCode("SMS_91865056");
//        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
//        //友情提示:如果JSON中需要带换行符,请参照标准的JSON协议对换行符的要求,比如短信内容中包含\r\n的情况在JSON中需要表示成\\r\\n,否则会导致JSON在服务端解析失败
//
//        String number = UIUtils.getNumber();
//        JSONObject jsonObj = new JSONObject();
//        try {
//            jsonObj.put("CODENO", number);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        request.setTemplateParam(jsonObj.toString());
//        //可选-上行短信扩展码(扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段)
//        //request.setSmsUpExtendCode("90997");
//        //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
//        request.setOutId("21212121211");
//        //请求失败这里会抛ClientException异常
//        SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
//        if (sendSmsResponse.getCode() != null && sendSmsResponse.getCode().equals("OK")) {
//            return number;
//        }
//        return "";
//    }
}
