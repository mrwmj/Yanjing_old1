package com.sdattg.yanjing.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.Utils.UIUtils;
import com.sdattg.yanjing.adapter.FmPagerAdapter;
import com.sdattg.yanjing.base.BaseActivity;
import com.sdattg.yanjing.fragment.me.MyAccountFragment;
import com.sdattg.yanjing.fragment.me.MySettingsFragment;
import com.sdattg.yanjing.fragment.me.PayFragment;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 个人中心
 * Created by Administrator on 2019/7/24.
 */

public class MeActivity extends BaseActivity {
    @Bind(R.id.tab_layout)
    TabLayout tabLayout;
    @Bind(R.id.view_pager)
    ViewPager viewPager;
    @Bind(R.id.iv_wen_hao)
    ImageView wenHao;
    private ArrayList<String> titles = new ArrayList<>();
    private MySettingsFragment settingsFragment;
    private MyAccountFragment accountFragment;
    private PayFragment payFragment;
    private ArrayList<Fragment> fragments = new ArrayList<>();
    private FmPagerAdapter pagerAdapter;

    @Override
    public int getLayout() {
        return R.layout.activity_me;
    }

    @Override
    public void initView() {
        setActionBarGone();
        titles.add("我的设置");
        titles.add("我的账号");
        titles.add("支付");
        for (int i = 0; i < titles.size(); i++) {
            tabLayout.addTab(tabLayout.newTab().setText(titles.get(i)));
        }

        settingsFragment = new MySettingsFragment();
        accountFragment = new MyAccountFragment();
        payFragment = new PayFragment();
        fragments.add(settingsFragment);
        fragments.add(accountFragment);
        fragments.add(payFragment);

        pagerAdapter = new FmPagerAdapter(getSupportFragmentManager(), titles, fragments);
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);


    }

    @Override
    public void initListener() {
        wenHao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UIUtils.showToast("问号");
            }
        });
    }

    @Override
    public void initData() {

    }
}
