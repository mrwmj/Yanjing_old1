package com.sdattg.yanjing.fragment;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.Utils.GlideImageLoader;
import com.sdattg.yanjing.base.BaseFragment;
import com.sdattg.yanjing.bean.CatalogBean;
import com.sdattg.yanjing.fragment.books.*;
import com.sdattg.yanjing.fragment.books.SKBaiKeFragment;
import com.sdattg.yanjing.view.titleview.PagerSlidingTabStrip;
import com.xuezj.cardbanner.ImageData;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by Administrator on 2019/7/24.
 */

public class ShuKuFragment extends BaseFragment {
    private static String TAG = "ShuKuFragment";
    @Bind(R.id.tab_layout)
    PagerSlidingTabStrip tabLayout;
    @Bind(R.id.view_pager)
    ViewPager viewPager;
    @Bind(R.id.banner)
    Banner banner;
    private Context mContext;
    private List<CatalogBean> list = new ArrayList<>();
    private ArrayList<Fragment> fragments = new ArrayList<>();

    @Override
    public int getLayout() {
        mContext = getContext();
        return R.layout.fragment_shu_ku;
    }

    @Override
    public void initView() {
        initBanner();
    }

    @Override
    public void initListener() {
    }

    @Override
    public void initData() {
        initTable();
        ShuKuPager adapter = new ShuKuPager(getChildFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setViewPager(viewPager);

    }

    /**
     * tab数据的初始化
     */
    private void initTable() {
        list.clear();
        fragments.clear();
        //查询书库下的分类
        Cursor cursor = dbManager.queryAll("secondary_catalog", "total_catalog_id=?", new String[]{"1"}, "secondary_catalog_id asc");
        while (cursor.moveToNext()) {
            String catalog_name = cursor.getString(cursor.getColumnIndex("catalog_name"));
            String secondary_catalog_id = cursor.getString(cursor.getColumnIndex("secondary_catalog_id"));
            String catalog_rank = cursor.getString(cursor.getColumnIndex("catalog_rank"));
            String total_catalog_id = cursor.getString(cursor.getColumnIndex("total_catalog_id"));
            CatalogBean catalogBean = new CatalogBean();
            catalogBean.setCatalog_name(catalog_name);
            catalogBean.setCatalog_id(secondary_catalog_id);
            catalogBean.setCatalog_rank(catalog_rank);
            catalogBean.setLast_catalog_id(total_catalog_id);
            list.add(catalogBean);
            if (secondary_catalog_id.equals("3")) {//灵修
                fragments.add(new SKLingXiuFragment(secondary_catalog_id));
            } else if (secondary_catalog_id.equals("4")) {//百科
                fragments.add(new SKBaiKeFragment(secondary_catalog_id));
            } else {
                fragments.add(new ShengJingFragment(secondary_catalog_id));
            }
        }
        cursor.close();
    }

    /**
     * 轮播图的初始化以及数据设置
     */
    private void initBanner() {
        List<Integer> imageDatas = new ArrayList();
        for (int i = 0; i < 5; i++) {
            ImageData imageData = new ImageData();
            imageData.setImage(R.drawable.bg_video);

            imageDatas.add(R.drawable.top_bg);
        }
        //设置图片加载器
        banner.setImageLoader(new GlideImageLoader());
        //设置banner动画效果
//        banner.setBannerAnimation(Transformer.DepthPage);
        //设置图片集合
        banner.setImages(imageDatas);
        //设置轮播时间
        banner.setDelayTime(2000);
        //设置指示器位置（当banner模式中有指示器时）
        banner.setIndicatorGravity(BannerConfig.RIGHT);
        //banner设置方法全部调用完毕时最后调用
        banner.start();
    }


    private class ShuKuPager extends FragmentPagerAdapter {
        private ShuKuPager(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public Fragment getItem(int position) {

            return fragments.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return list.get(position).getCatalog_name();
        }
    }

    //如果你需要考虑更好的体验，可以这么操作
    @Override
    public void onStart() {
        super.onStart();
        //开始轮播
        banner.startAutoPlay();
    }

    @Override
    public void onStop() {
        super.onStop();
        //结束轮播
        banner.stopAutoPlay();
    }

}
