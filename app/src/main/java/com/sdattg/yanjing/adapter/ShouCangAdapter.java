package com.sdattg.yanjing.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.bean.zuji.ShouCangBean;
import com.sdattg.yanjing.fragment.zuji.CollectFragment;
import com.sdattg.yanjing.view.SlideRecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2019/7/26.
 */

public class ShouCangAdapter extends RecyclerView.Adapter<ShouCangAdapter.ShouCangHolder> {
    private static String TAG = "HistoryAdapter";


    private List<ShouCangBean> beans = new ArrayList<>();
    private Context mContext;
    private CollectFragment collectFragment;
    private SlideRecyclerView recyclerView;
    private Map<Integer, Boolean> map = new HashMap<>();

    public ShouCangAdapter(Context mContext, CollectFragment collectFragment, SlideRecyclerView recyclerView) {
        this.mContext = mContext;
        this.collectFragment = collectFragment;
        this.recyclerView = recyclerView;
    }

    public void setData(List<ShouCangBean> beans) {
        this.beans.clear();
        this.beans.addAll(beans);
        notifyDataSetChanged();
    }

    public void addData(List<ShouCangBean> beans) {
        this.beans.addAll(beans);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ShouCangHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(mContext).inflate(R.layout.adapter_history_item, parent, false);
        ShouCangHolder videoHolder = new ShouCangHolder(inflate);
        return videoHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ShouCangHolder holder, final int position) {
        final ShouCangBean bean = beans.get(position);
        holder.tvBooksMinName.setText(bean.min_name);
        holder.tvBooksName.setText(bean.book_name);
        holder.tvTime.setText(bean.time);
        holder.tvBooksInfo.setText(bean.chapter_name);

        //是否显示置顶
        if (bean.is_collect == 0) {
            holder.ivZhiding.setVisibility(View.VISIBLE);
            holder.jianyi_zhiding.setText("取消置顶");
        } else {
            holder.ivZhiding.setVisibility(View.GONE);
            holder.jianyi_zhiding.setText("置顶");
        }

        //显示清空所有
        if (map.size() == beans.size()) {
            if (map.get(position)) {
                holder.checkBox.setChecked(true);
            } else {
                holder.checkBox.setChecked(false);
            }
            holder.checkBox.setVisibility(View.VISIBLE);
        } else {
            holder.checkBox.setVisibility(View.GONE);
        }


        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (map.size() == beans.size()) {
                    map.put(position, isChecked);
                }
            }
        });


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickListener != null) {
                    onClickListener.onClick(position);
                }
            }
        });

        holder.jianyi_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.closeMenu();
                collectFragment.deleteRecord(bean);
            }
        });

        holder.jianyi_zhiding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.closeMenu();
                if (bean.is_collect == 0) {
                    collectFragment.cancelTopping(bean);
                } else {
                    collectFragment.topping(bean);
                }
            }
        });


    }

    public OnClickListener onClickListener;

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public interface OnClickListener {
        void onClick(int position);
    }


    public void setTop(int adapterPosition) {
        ShouCangBean bean = beans.get(adapterPosition);
        beans.remove(bean);
        beans.add(0, bean);
        notifyItemMoved(adapterPosition, 0);
    }

    @Override
    public int getItemCount() {
        return beans.size();
    }


    public void initChebox(int itemCount) {
        for (int i = 0; i < itemCount; i++) {
            map.put(i, true);
        }
    }

    public void clearChebox() {
        map.clear();

    }

    public Map<Integer, Boolean> getCheboxMap() {
        return map;
    }

    public ShouCangBean getDeleteBean(int position) {
        return beans.get(position);
    }


    public class ShouCangHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_books_min_name)
        TextView tvBooksMinName;
        @Bind(R.id.tv_books_name)
        TextView tvBooksName;
        @Bind(R.id.tv_time)
        TextView tvTime;
        @Bind(R.id.tv_books_info)
        TextView tvBooksInfo;

        @Bind(R.id.iv_zhi_ding)
        ImageView ivZhiding;
        @Bind(R.id.check_box)
        CheckBox checkBox;
        
        @Bind(R.id.jianyi_zhiding)
        TextView jianyi_zhiding;
        @Bind(R.id.jianyi_delete)
        TextView jianyi_delete;

        public ShouCangHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

