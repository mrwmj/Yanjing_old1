package com.sdattg.yanjing;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by 10196 on 2019/6/9.
 */

public class TestActivity extends AppCompatActivity {
    Button copy_to_sdcard;
    Button unzip;
    Button send_file;
    ProgressDialog dialog;
    WebView webView;
    public static final String TAG = "ZIP";
    //设置解压目的路径
    public static String OUTPUT_DIRECTORY = Environment
            .getExternalStorageDirectory().getAbsolutePath() + "/output2";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_test);
        copy_to_sdcard = findViewById(R.id.copy_to_sdcard);
        unzip = findViewById(R.id.unzip);
        send_file = findViewById(R.id.send_file);



        webView = findViewById(R.id.webviewtest);
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDefaultTextEncodingName("UTF-8");
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.setWebChromeClient(new WebChromeClient());
        WebViewClient webViewClient = new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // 如下方案可在非微信内部WebView的H5页面中调出微信支付
                if (url.startsWith("weixin://wap/pay?")) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                    return true;
                } else {
                    Map<String, String> extraHeaders = new HashMap<String, String>();
                    extraHeaders.put("Referer", "http://wxpay.wxutil.com");
                    view.loadUrl(url, extraHeaders);
                }
                return true;
            }
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, android.net.http.SslError error) { // 重写此方法可以让webview处理https请求
                handler.proceed();
            }
        };
        webView.setWebViewClient(webViewClient);
        webView.loadUrl("https://wxpay.wxutil.com/mch/pay/h5.v2.php");




        dialog = new ProgressDialog(TestActivity.this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy( builder.build() );
        }

        if(Build.VERSION.SDK_INT >= 23){
            checkPermission();
        }
        copy_to_sdcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.setTitle("提示");
                dialog.setMessage("正在复制文件，请稍后！");
                dialog.show();//显示对话框
                new Thread() {
                    public void run() {
                        //在新线程中以同名覆盖方式解压
                        try {
                            CopyFileFromAssets.copy(TestActivity.this, "666.zip", OUTPUT_DIRECTORY, "777.zip"); //ZIP写入SD卡
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        dialog.cancel();//解压完成后关闭对话框
                        Looper.prepare();
                        Toast.makeText(TestActivity.this, "复制完成", Toast.LENGTH_SHORT).show();
                        Looper.loop();
                    }
                }.start();
            }
        });

        unzip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.setTitle("提示");
                dialog.setMessage("正在解压文件，请稍后！");
                dialog.show();//显示对话框
                new Thread() {
                    @Override
                    public void run() {
                        try {
                            ZipUtil.unZip("/storage/sdcard0/output2/777.zip", OUTPUT_DIRECTORY, "");//读取ZIP
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        dialog.cancel();//解压完成后关闭对话框
                        Looper.prepare();
                        Toast.makeText(TestActivity.this, "解压完成", Toast.LENGTH_SHORT).show();
                        Looper.loop();
                    }
                }.start();
            }
        });
        send_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFile(TestActivity.this);
            }
        });
    }
    /**
     * 通过蓝牙发送文件
     */
    private void sendFile(Activity activity) {
        PackageManager localPackageManager = activity.getPackageManager();
        Intent localIntent = null;

        HashMap<String, ActivityInfo> localHashMap = null;

        try {
            localIntent = new Intent();
            localIntent.setAction(Intent.ACTION_SEND);
            File file = new File(OUTPUT_DIRECTORY+"/777.zip");
            localIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            // localIntent.putExtra(Intent.EXTRA_STREAM,
            // Uri.fromFile(new File(localApplicationInfo.sourceDir)));
            localIntent.setType("*/*");
            List<ResolveInfo> localList = localPackageManager.queryIntentActivities(
                    localIntent, 0);
            localHashMap = new HashMap<String, ActivityInfo>();
            Iterator<ResolveInfo> localIterator1 = localList.iterator();
            while (localIterator1.hasNext()) {
                ResolveInfo resolveInfo = (ResolveInfo) localIterator1.next();
                ActivityInfo localActivityInfo2 = resolveInfo.activityInfo;
                String str = localActivityInfo2.applicationInfo.processName;
                if (str.contains("bluetooth"))
                    localHashMap.put(str, localActivityInfo2);
            }
        } catch (Exception localException) {
//            ToastHelper.showBlueToothSupportErr(activity);
        }
        if (localHashMap.size() == 0){
//            ToastHelper.showBlueToothSupportErr(activity);
        }
        ActivityInfo localActivityInfo1 = (ActivityInfo) localHashMap
                .get("com.android.bluetooth");
        if (localActivityInfo1 == null) {
            localActivityInfo1 = (ActivityInfo) localHashMap
                    .get("com.mediatek.bluetooth");
        }
        if (localActivityInfo1 == null) {
            Iterator<ActivityInfo> localIterator2 = localHashMap.values().iterator();
            if (localIterator2.hasNext())
                localActivityInfo1 = (ActivityInfo) localIterator2.next();
        }
        if (localActivityInfo1 != null) {
            localIntent.setComponent(new ComponentName(
                    localActivityInfo1.packageName, localActivityInfo1.name));
            activity.startActivityForResult(localIntent, 4098);
            return;
        }
//        ToastHelper.showBlueToothSupportErr(activity);
    }
    void checkPermission() {
        //检查权限（NEED_PERMISSION）是否被授权 PackageManager.PERMISSION_GRANTED表示同意授权
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            //用户已经拒绝过一次，再次弹出权限申请对话框需要给用户一个解释
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission
                    .WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(this,"请开通相关权限，否则无法正常使用本应用！",Toast.LENGTH_SHORT).show();
            }
            //申请权限
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);

        } else {
//            ToastUtil.showToast("授权成功！");

        }
    }

}
