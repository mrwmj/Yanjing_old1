package com.sdattg.yanjing.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.Utils.DbUtils;
import com.sdattg.yanjing.Utils.UIUtils;
import com.sdattg.yanjing.activity.FootprintActivity;
import com.sdattg.yanjing.bean.BooksBean;
import com.sdattg.yanjing.bean.zuji.HistoryRecordBean;
import com.sdattg.yanjing.bean.zuji.ShuQianBean;
import com.sdattg.yanjing.fragment.zuji.BookmarkFragment;

import org.xutils.ex.DbException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 书签item适配器
 * Created by Administrator on 2019/7/26.
 */

public class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.BookmarkHolder> {
    private static String TAG = "HistoryAdapter";


    private List<ShuQianBean> beans = new ArrayList<>();
    private Context mContext;
    private FootprintActivity activity;
    private FragmentManager fragmentManager;
    private BookmarkFragment bookmarkFragment;

    private LinkedList<Boolean> linkedList = new LinkedList<>();
    private Map<Integer, Boolean> map = new HashMap<>();

    public BookmarkAdapter(Context mContext, FootprintActivity activity, BookmarkFragment bookmarkFragment) {
        this.mContext = mContext;
        this.activity = activity;
        this.bookmarkFragment = bookmarkFragment;
        this.fragmentManager = bookmarkFragment.getFragmentManager();
    }

    public void setData(List<ShuQianBean> beans) {
        this.beans.clear();
        this.beans.addAll(beans);
        linkedList.clear();
        notifyDataSetChanged();
    }

    public void addData(ArrayList<ShuQianBean> beans) {
        this.beans.addAll(beans);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BookmarkHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(mContext).inflate(R.layout.adapter_bookmark_item, parent, false);
        BookmarkHolder videoHolder = new BookmarkHolder(inflate);
        return videoHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final BookmarkHolder holder, final int position) {
        final ShuQianBean bean = beans.get(position);

        holder.tvBooksName.setText(bean.book_name);
        holder.tvChapterName.setText(bean.chapter_name);
        holder.tvCatalog.setText(bean.catalog_name);

        holder.tvBooksInfo.setText("\u3000\u3000" + bean.collection_content);
        holder.tvLeaveComments.setText(bean.leave_comments);//留言
        holder.tvTime.setText(bean.time);
        if (bean.open) {
            holder.ivJianTou.setImageResource(R.drawable.guan_bi);
            holder.llOperate.setVisibility(View.VISIBLE);
        } else {
            holder.ivJianTou.setImageResource(R.drawable.jiantou_left);
            holder.llOperate.setVisibility(View.GONE);
        }

        holder.ivJianTou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bean.open) {
                    bean.open = false;
                    holder.llOperate.setVisibility(View.GONE);
                    holder.ivJianTou.setImageResource(R.drawable.jiantou_left);
                } else {
                    bean.open = true;
                    holder.llOperate.setVisibility(View.VISIBLE);
                    holder.ivJianTou.setImageResource(R.drawable.guan_bi);
                }

            }
        });
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEdit(bean);
            }
        });

        holder.ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UIUtils.showToast("分享");
            }
        });

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    DbUtils.getDB().delete(bean);
                    beans.remove(bean);
                    activity.upTabBadge(2, beans.size());
                    bookmarkFragment.setHintLlDelete();
                    notifyDataSetChanged();
                } catch (DbException e) {
                    e.printStackTrace();
                }
                notifyDataSetChanged();
            }
        });
        //显示清空所有
        if (map.size() == beans.size()) {
            if (map.get(position)) {
                holder.checkBox.setChecked(true);
            } else {
                holder.checkBox.setChecked(false);
            }
            holder.checkBox.setVisibility(View.VISIBLE);
        } else {
            holder.checkBox.setVisibility(View.GONE);
        }


        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (map.size() == beans.size()) {
                    map.put(position, isChecked);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return beans.size();
    }


    public void initChebox(int itemCount) {
        for (int i = 0; i < itemCount; i++) {
            map.put(i, true);
        }
    }

    public void clearChebox() {
        map.clear();

    }

    public Map<Integer, Boolean> getCheboxMap() {
        return map;
    }

    public ShuQianBean getDeleteBean(int position) {
        return beans.get(position);
    }

    public class BookmarkHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_books_name)
        TextView tvBooksName;
        @Bind(R.id.tv_chapter_name)
        TextView tvChapterName;
        @Bind(R.id.tv_catalog)
        TextView tvCatalog;
        @Bind(R.id.tv_books_info)
        TextView tvBooksInfo;
        @Bind(R.id.tv_leave_comments)
        TextView tvLeaveComments;
        @Bind(R.id.tv_time)
        TextView tvTime;

        @Bind(R.id.iv_edit)
        ImageView ivEdit;
        @Bind(R.id.iv_share)
        ImageView ivShare;
        @Bind(R.id.iv_delete)
        ImageView ivDelete;
        @Bind(R.id.ll_operate)
        LinearLayout llOperate;
        @Bind(R.id.iv_jian_tou)
        ImageView ivJianTou;

        @Bind(R.id.check_box)
        CheckBox checkBox;

        public BookmarkHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    private AlertDialog alertDialog;
    private ImageView ivBack;//返回
    private TextView booksName;
    private TextView chapterName;
    private TextView booksInfo;
    private TextView catalog;
    private EditText leaveComments;
    private TextView submit;

    private void showEdit(final ShuQianBean bean) {
        View view = View.inflate(mContext, R.layout.dialog_book_make_edit, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext)
                .setView(view);
        alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCancelable(false);//设置外部点击不可取消
        //返回键监听
        alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (alertDialog != null)
                        alertDialog.dismiss();
                    return true;
                } else {
                    return false;
                }
            }
        });
        Window window = alertDialog.getWindow();
        WindowManager wm = window.getWindowManager();
        Point windowSize = new Point();
        wm.getDefaultDisplay().getSize(windowSize);
        float size_x = 0.95f;
        float size_y = 0.9f;
        int width = windowSize.x;
        int height = windowSize.y;
        if (width >= height) {// 横屏
            window.getAttributes().width = (int) (windowSize.y * size_x * 1.1);
            window.getAttributes().height = (int) (windowSize.y * size_y);
        } else {// 竖屏
            window.getAttributes().width = (int) (windowSize.x * size_x);
            window.getAttributes().height = (int) (windowSize.x * size_y);
        }
        window.setGravity(Gravity.CENTER);


        ivBack = view.findViewById(R.id.iv_back);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog != null)
                    alertDialog.dismiss();
            }
        });
        booksName = view.findViewById(R.id.tv_books_name);
        chapterName = view.findViewById(R.id.tv_chapter_name);
        booksInfo = view.findViewById(R.id.tv_books_info);
        catalog = view.findViewById(R.id.tv_catalog);
        leaveComments = view.findViewById(R.id.et_leave_comments);
        submit = view.findViewById(R.id.tv_submit);


        booksName.setText(bean.book_name);
        chapterName.setText(bean.chapter_name);
        catalog.setText(bean.catalog_name);
        booksInfo.setText("\u3000\u3000" + bean.collection_content);//首行缩进2个字符
        leaveComments.setText(bean.leave_comments);//留言


        leaveComments.setSelection(leaveComments.getText().length());
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String trim = leaveComments.getText().toString().trim();
                bean.leave_comments = trim;
                bean.open = false;
                try {
                    DbUtils.getDB().saveOrUpdate(bean);
                    if (alertDialog != null) {
                        alertDialog.dismiss();
                    }
                    UIUtils.showToast("保存成功");
                    notifyDataSetChanged();
                } catch (DbException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}

