package com.sdattg.yanjing.bean;

/**
 * Created by Administrator on 2019/7/29.
 */

public class VideoBean {
    private int url;
    private String name;
    private String info;

    public int getUrl() {
        return url;
    }

    public void setUrl(int url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "VideoBean{" +
                "url=" + url +
                ", name='" + name + '\'' +
                ", info='" + info + '\'' +
                '}';
    }
}
