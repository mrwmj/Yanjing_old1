package com.sdattg.yanjing.fragment.books;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.adapter.FmPagerAdapter;
import com.sdattg.yanjing.base.BaseFragment;
import com.sdattg.yanjing.bean.CatalogBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * 圣经
 * Created by Administrator on 2019/7/24.
 */

public class ShengJingFragment extends BaseFragment {
    private static String TAG = "ShengJingFragment";
    @Bind(R.id.tab_layout)
    TabLayout tabLayout;
    @Bind(R.id.view_pager)
    ViewPager viewPager;

    private Context mContext;
    private FmPagerAdapter pagerAdapter;

    private List<CatalogBean> list = new ArrayList<>();
    private ArrayList<String> tabs = new ArrayList<>();
    private ArrayList<Fragment> fragments = new ArrayList<>();
    private String type = "1";

    public ShengJingFragment() {
    }

    @SuppressLint("ValidFragment")
    public ShengJingFragment(String type) {
        this.type = type;
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_sheng_jing;
    }

    @Override
    public void initView() {
        mContext = getContext();

    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {
        list.clear();
        fragments.clear();
        CatalogBean catalogBean = new CatalogBean();
        catalogBean.setCatalog_name("全部");
        catalogBean.setCatalog_id("-1");
        catalogBean.setCatalog_rank("-1");
        catalogBean.setLast_catalog_id(type);
        list.add(catalogBean);
        tabs.add("全部");
        Cursor cursor = dbManager.queryAll("third_level_catalog", "secondary_catalog_id=?", new String[]{type}, "third_level_catalog asc");
        while (cursor.moveToNext()) {
            String catalog_name = cursor.getString(cursor.getColumnIndex("catalog_name"));
            String secondary_catalog_id = cursor.getString(cursor.getColumnIndex("secondary_catalog_id"));
            String catalog_rank = cursor.getString(cursor.getColumnIndex("catalog_rank"));
            String third_level_catalog = cursor.getString(cursor.getColumnIndex("third_level_catalog"));

            CatalogBean catalogBean2 = new CatalogBean();
            catalogBean2.setCatalog_name(catalog_name);
            catalogBean2.setCatalog_id(third_level_catalog);
            catalogBean2.setCatalog_rank(catalog_rank);
            catalogBean2.setLast_catalog_id(secondary_catalog_id);
            list.add(catalogBean2);
            tabs.add(catalog_name);
        }

        cursor.close();

        for (int i = 0; i < list.size(); i++) {
            tabLayout.addTab(tabLayout.newTab());
            fragments.add(new SJ_XinYueFragment(list.get(i)));
        }
        for (int i = 0; i < list.size(); i++) {
            tabLayout.getTabAt(i).setText(list.get(i).getCatalog_name());
        }
        pagerAdapter = new FmPagerAdapter(ShengJingFragment.this.getChildFragmentManager(), tabs, fragments);
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);//将TabLayout与Viewpager联动起来
        tabLayout.setTabsFromPagerAdapter(pagerAdapter);
    }
}
