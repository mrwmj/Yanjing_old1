package com.sdattg.yanjing.config;

/**
 * 定义所有访问sharedpreference的key
 */
public interface ConfigKey {

    /***
     * 首次登录
     */
    String FIRST = "is_first";


    /**
     * 登录账号
     */
    String LOGIN_ACCOUNT = "login_account";
    /**
     * 登录密码
     */
    String PASSWORD = "password";

    /**
     * 搜索方式
     */

    String SEARCH_MODE = "search_mode";

    /**
     * 主题颜色
     */
    String THEME_COLOR = "theme_color";


    /**
     * 展示样式
     */
    String SHOW_STYLE = "show_style";
}
