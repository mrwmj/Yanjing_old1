package com.sdattg.yanjing.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.bean.zuji.TabTitlesBean;
import com.sdattg.yanjing.view.LabelView;

import java.util.ArrayList;

/**
 * Created by Administrator on 2019/8/23.
 */

public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private ArrayList<Fragment> mFragmentList;
    private ArrayList<TabTitlesBean> mPageTitleList;

    public SimpleFragmentPagerAdapter(Context context,
                                      FragmentManager fm,
                                      ArrayList<Fragment> fragmentList,
                                      ArrayList<TabTitlesBean> pageTitleList) {
        super(fm);
        this.mContext = context;
        this.mFragmentList = fragmentList;
        this.mPageTitleList = pageTitleList;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mPageTitleList.get(position).title_name;
    }

    public View getTabItemView(int position) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.tab_layout_item, null);
        TextView textView = (TextView) view.findViewById(R.id.text_view);
        TextView tvCount = (TextView) view.findViewById(R.id.tv_count);
        tvCount.setText(formatBadgeNumber(mPageTitleList.get(position).count));
        textView.setText(mPageTitleList.get(position).title_name);
        return view;
    }

    public static String formatBadgeNumber(int value) {
        if (value <= 0) {
            return Integer.toString(0);
        }
        if (value < 100) {
            return Integer.toString(value);
        }
        return "99+";
    }
}