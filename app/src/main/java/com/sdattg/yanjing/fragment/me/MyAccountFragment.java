package com.sdattg.yanjing.fragment.me;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.base.BaseFragment;

/**
 * Created by Administrator on 2019/7/28.
 */

public class MyAccountFragment extends BaseFragment{
    @Override
    public int getLayout() {
        return R.layout.fragment_my_account;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {

    }
}
