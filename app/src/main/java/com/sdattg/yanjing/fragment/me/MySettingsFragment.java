package com.sdattg.yanjing.fragment.me;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.Utils.UIUtils;
import com.sdattg.yanjing.base.BaseFragment;
import com.sdattg.yanjing.common.Constant;
import com.sdattg.yanjing.config.ConfigKey;

import org.xutils.ex.DbException;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2019/7/28.
 */

public class MySettingsFragment extends BaseFragment {

    @Bind(R.id.btn_delete)
    Button btnDelete;

    @Bind(R.id.rb_lie_biao)
    RadioButton rbLieBiao;
    @Bind(R.id.rb_jiu_gong_ge)
    RadioButton rbJiuGongGe;
    @Bind(R.id.rg_home_show_type)
    RadioGroup rgHomeShowType;

    @Override
    public int getLayout() {
        return R.layout.fragment_my_setting;
    }

    @Override
    public void initView() {
        String string = mConfigUtils.getString(ConfigKey.SHOW_STYLE, Constant.LIE_BIAO);
        if (string.equals(Constant.LIE_BIAO)) {
            rgHomeShowType.check(R.id.rb_lie_biao);
//            rbLieBiao.setChecked(true);
        } else {
            rgHomeShowType.check(R.id.rb_jiu_gong_ge);
//            rbJiuGongGe.setChecked(true);
        }


    }

    @Override
    public void initListener() {
        rgHomeShowType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_lie_biao:
                        mConfigUtils.put(ConfigKey.SHOW_STYLE, Constant.LIE_BIAO);
                        break;
                    case R.id.rb_jiu_gong_ge:
                        mConfigUtils.put(ConfigKey.SHOW_STYLE, Constant.JIU_GONG_GE);
                        break;
                }
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    db.dropDb();
                    UIUtils.showToast("清除成功");
                } catch (DbException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void initData() {

    }

}
