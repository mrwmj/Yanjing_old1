package com.sdattg.yanjing.fragment.zuji;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.Utils.DbUtils;
import com.sdattg.yanjing.Utils.UIUtils;
import com.sdattg.yanjing.activity.FootprintActivity;
import com.sdattg.yanjing.adapter.BookmarkAdapter;
import com.sdattg.yanjing.base.BaseFragment;
import com.sdattg.yanjing.bean.zuji.ShouCangBean;
import com.sdattg.yanjing.bean.zuji.ShuQianBean;
import com.sdattg.yanjing.dialog.ConfirmDialog;

import org.xutils.DbManager;
import org.xutils.db.Selector;
import org.xutils.db.sqlite.WhereBuilder;
import org.xutils.ex.DbException;

import java.util.List;
import java.util.Map;

import butterknife.Bind;

/**
 * 我的书签
 * Created by Administrator on 2019/7/29.
 */

public class BookmarkFragment extends BaseFragment {
    private static String TAG = "BookmarkFragment";
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.btn_add_data)
    Button addData;
    @Bind(R.id.et_search)
    EditText etSearch;
    @Bind(R.id.tv_search)
    TextView tvSearch;
    @Bind(R.id.tv_search_num)
    TextView tvSearchNum;
    @Bind(R.id.iv_sequence)
    ImageView ivSequence;

    @Bind(R.id.ll_delete)
    LinearLayout llDelete;
    @Bind(R.id.tv_delete)
    TextView tvDelete;
    @Bind(R.id.tv_cancel)
    TextView tvCancel;


    private List<ShuQianBean> list = null;
    private BookmarkAdapter bookmarkAdapter;
    private DbManager db;
    private FootprintActivity activity;

    private boolean isDescending = true;
    public boolean is_show_delete = false;
    private ConfirmDialog confirmDialog;

    @Override
    public int getLayout() {
        return R.layout.fragment_bookmark;
    }

    @Override
    public void initView() {
        db = DbUtils.getDB();

        activity = (FootprintActivity) getActivity();
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext().getApplicationContext()));

        bookmarkAdapter = new BookmarkAdapter(getContext(), activity, this);
        recyclerView.setAdapter(bookmarkAdapter);

    }

    @Override
    public void initListener() {
        addData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addData();
            }
        });


        tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDialog = new ConfirmDialog.Builder()
                        .setTtile("提示")
                        .setContent("你确定要删除所有“我的书签”吗？不可恢复！")
                        .setOnCancleListener(onCancleListener)
                        .setOnConfirmListener(onConfirmListener)
                        .show(getActivity(), getFragmentManager());
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                is_show_delete = false;
                if (bookmarkAdapter != null) {
                    bookmarkAdapter.clearChebox();
                    bookmarkAdapter.notifyDataSetChanged();
                }
                llDelete.setVisibility(View.GONE);
                addData.setVisibility(View.VISIBLE);
                activity.setClearAllText("清空所有");
            }
        });

        ivSequence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDescending) {
                    //降序排序
                    isDescending = false;
                    ivSequence.setImageResource(R.drawable.sheng_xu2);


                    try {
                        list = db.selector(ShuQianBean.class).orderBy("id", isDescending).findAll();
                        if (list != null) {
                            bookmarkAdapter.setData(list);
                            activity.upTabBadge(2, list.size());
                        }
                    } catch (DbException e) {
                        e.printStackTrace();
                    }
                } else {
                    isDescending = true;
                    ivSequence.setImageResource(R.drawable.jiang_xu2);

                    try {

                        list = db.selector(ShuQianBean.class).orderBy("id", isDescending).findAll();

                        if (list != null) {
                            bookmarkAdapter.setData(list);
                            activity.upTabBadge(2, list.size());
                        }
                    } catch (DbException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = etSearch.getText().toString().trim();
                if (TextUtils.isEmpty(text)) {
                    UIUtils.showToast("请输入搜索内容");
                    return;
                }

                try {

                    Selector<ShuQianBean> selector = db.selector(ShuQianBean.class)
                            .where(WhereBuilder.b("collection_content", "like", "%" + text + "%"));
                    list = selector.findAll();
                    if (list != null) {
                        if (list.size() > 0) {
                            tvSearchNum.setText(list.size() + "");
                            tvSearchNum.setVisibility(View.VISIBLE);
                            bookmarkAdapter.setData(list);
                        } else {
                            tvSearchNum.setVisibility(View.INVISIBLE);
                            tvSearchNum.setText("0");
                            bookmarkAdapter.setData(list);
                            UIUtils.showToast("没有搜索到相关内容");
                        }

                    } else {
                        UIUtils.showToast("没有搜索到相关内容");
                        tvSearchNum.setVisibility(View.INVISIBLE);
                        tvSearchNum.setText("0");
                    }


                } catch (DbException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private View.OnClickListener onCancleListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (confirmDialog != null) {
                confirmDialog.dismiss();
            }
        }
    };
    private View.OnClickListener onConfirmListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (confirmDialog != null) {
                confirmDialog.dismiss();
                if (bookmarkAdapter != null) {
                    Map<Integer, Boolean> cheboxMap = bookmarkAdapter.getCheboxMap();
                    for (int key : cheboxMap.keySet()) {
                        if (cheboxMap.get(key)) {
                            ShuQianBean deleteBean = bookmarkAdapter.getDeleteBean(key);
                            try {
                                db.delete(deleteBean);
                            } catch (DbException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    UIUtils.showToast("删除成功");
                    activity.setClearAllText("清空所有");
                    quanXuan(false);
                    initData();
                }
            }

        }
    };

    @Override
    public void initData() {
        try {

            list = db.selector(ShuQianBean.class).orderBy("id", isDescending).findAll();

            if (list != null) {
                activity.upTabBadge(2, list.size());
                bookmarkAdapter.setData(list);
                bookmarkAdapter.clearChebox();

            }
        } catch (DbException e) {
            e.printStackTrace();
        }
    }


    private void addData() {
        ShuQianBean bean = new ShuQianBean();
        bean.book_name = "健康呼召";
        bean.chapter_name = "第一章 服务的呼召";
        bean.catalog_name = "怀著-证言";
        bean.collection_content = "耶稣快到耶路撒冷，看见城，就为它哀哭，说：巴不得你在这日子知道关系你平安的事，无奈这事现在是隐藏的，叫你的眼看不出来。因为日子将到，你的仇敌必筑起土垒，周围环绕你，四面困住你，并要扫灭你和你里头的儿女，连一块石头也不留在石头上，因你不知道眷顾你的时候。" + System.currentTimeMillis();
        bean.leave_comments = "留言留言留言留言留言留言留言留言留言留言留言留言";
        bean.open = false;
        bean.time = UIUtils.getDateStr2(System.currentTimeMillis());
        try {
            db.saveOrUpdate(bean);
            initData();
        } catch (DbException e) {
            e.printStackTrace();
        }
    }


    /**
     * 显示清除所有/取消删除
     */
    public void quanXuan(boolean show_delete) {
        is_show_delete = show_delete;
        if (bookmarkAdapter != null) {
            if (is_show_delete) {
                bookmarkAdapter.initChebox(bookmarkAdapter.getItemCount());
            }
            bookmarkAdapter.notifyDataSetChanged();
        }
        if (is_show_delete) {
            llDelete.setVisibility(View.VISIBLE);
            addData.setVisibility(View.GONE);
        } else {
            llDelete.setVisibility(View.GONE);
            addData.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 反选
     */
    public void fanXuan() {
        if (bookmarkAdapter != null) {
            Map<Integer, Boolean> cheboxMap = bookmarkAdapter.getCheboxMap();
            for (int key : cheboxMap.keySet()) {
                cheboxMap.put(key, !cheboxMap.get(key));
            }
            bookmarkAdapter.notifyDataSetChanged();
        }
    }


    public void setHintLlDelete() {
        llDelete.setVisibility(View.GONE);
        addData.setVisibility(View.VISIBLE);
        activity.setClearAllText("清空所有");
        is_show_delete = false;
    }
}
