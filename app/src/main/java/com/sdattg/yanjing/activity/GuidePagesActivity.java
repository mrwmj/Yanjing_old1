package com.sdattg.yanjing.activity;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.base.BaseActivity;
import com.sdattg.yanjing.config.ConfigKey;

import butterknife.Bind;

/**
 * 欢迎页
 * Created by Administrator on 2019/7/11.
 */

public class GuidePagesActivity extends BaseActivity {
    @Bind(R.id.tv_start)
    TextView tvStart;


    @Override
    public int getLayout() {
        return R.layout.activity_guide_pages;
    }

    @Override
    public void initView() {
        setActionBarGone();
    }

    @Override
    public void initListener() {
        tvStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mConfigUtils.put(ConfigKey.FIRST, false);
                startActivity(new Intent(GuidePagesActivity.this, LoginActivity.class));
                finish();
            }
        });
    }

    @Override
    public void initData() {

        boolean first = mConfigUtils.getBoolean(ConfigKey.FIRST, true);

        if (!first) {
            startActivity(new Intent(GuidePagesActivity.this, LoginActivity.class));
            finish();
        }
    }
}
