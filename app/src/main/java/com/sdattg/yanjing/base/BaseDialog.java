package com.sdattg.yanjing.base;


import android.app.DialogFragment;
import android.os.Bundle;

import com.sdattg.yanjing.config.ConfigUtils;

/**
 * Created by Administrator on 2019/7/3.
 */

public class BaseDialog extends DialogFragment {
    /**
     * SharedPreferences工具
     */
    public ConfigUtils mConfig;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mConfig = new ConfigUtils(getActivity());
    }
}
