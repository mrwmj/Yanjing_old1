package com.sdattg.yanjing.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sdattg.yanjing.R;
import com.sdattg.yanjing.bean.VideoBean;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2019/7/26.
 */

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoHolder> {
    private static String TAG = "VideoAdapter";


    private ArrayList<VideoBean> videoBeans = new ArrayList<>();
    private Context mContext;

    public VideoAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setVideo(ArrayList<VideoBean> videoBeans) {
        this.videoBeans.clear();
        this.videoBeans.addAll(videoBeans);
        notifyDataSetChanged();
    }

    public void addVideo(ArrayList<VideoBean> videoBeans) {
        this.videoBeans.addAll(videoBeans);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VideoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(mContext).inflate(R.layout.adapter_video_item, parent, false);
        VideoHolder videoHolder = new VideoHolder(inflate);
        return videoHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull VideoHolder holder, int position) {
        VideoBean videoBean = videoBeans.get(position);


//        Glide.with(mContext).load(videoBean.getUrl()).into(holder.ivUrl);
//        Glide.with(mContext).load(videoBean.getImg()).into(holder.ivPlay);

        holder.tvName.setText(videoBean.getName());
        holder.tvInfo.setText(videoBean.getInfo());
    }

    @Override
    public int getItemCount() {
        return videoBeans.size();
    }


    public class VideoHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_name)
        TextView tvName;
        @Bind(R.id.tv_info)
        TextView tvInfo;

        public VideoHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

