package com.sdattg.yanjing.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Administrator on 2017/9/29.
 */

public class DBHelper extends SQLiteOpenHelper {
    private static String TAG = "DBHelper";

    //总的目录
    private static String TOTAL_CATALOG = "create table total_catalog(_id integer primary key autoincrement," +
            "catalog_name text," +
            "catalog_rank integer," +  //目录等级
            "total_catalog_id integer)";

    //二级目录
    private static String SECONDARY_CATALOG = "create table secondary_catalog(_id integer primary key autoincrement," +
            "catalog_name text," +
            "secondary_catalog_id integer," +
            "catalog_rank integer," +  //目录等级
            "total_catalog_id integer)";

    //三级目录
    private static String THIRD_LEVEL_CATALOG = "create table third_level_catalog(_id integer primary key autoincrement," +
            "catalog_name text," +
            "secondary_catalog_id integer," +
            "catalog_rank integer," +  //目录等级
            "third_level_catalog integer)";

    //书籍
    private static String BOOKS = "create table books(_id integer primary key autoincrement," +
            "books_name text," +  //书名
            "books_min_name text," +//缩略书名
            "books_version text," + //版本
            "books_summary text," +//书籍简介
            "books_id integer," +//书籍id
            "catalog_rank integer," +//所属目录层级
            "catalog_id integer)";//所属目录id

    //章节
    private static String CHAPTER = "create table chapter(_id integer primary key autoincrement," +
            "chapter_name text," +  //章节名
            "chapter_id text," +//章节id
            "chapter_annotate text," + //章节注释
            "context text," + //内容
            "books_id integer)";//所属书籍id


    //书籍内容
    private static String BOOK_CONTENT = "create table book_content(_id integer primary key autoincrement," +
            "books_id integer," +  //书籍id
            "chapter_id integer," +//章节id
            "content text," + //内容
            "books_version text)";//书籍版本
    //    //百科
//    private static final String BAIKE = "create table baike(_id integer primary key autoincrement," +
//            "name text," +
//            "index_id integer," +
//            "content text," +
//            "category_id integer," +//分类id   和baike_category表category_id对应
//            "cate_name text)";      //分类名
//    //百科类别
//    private static final String BAIKE_CATEGORY = "create table baike_category(_id integer primary key autoincrement," +
//            "cate_name text," +     //分类名
//            "vol_count integer," + //每类数量
//            "category_id integer," + //分类id
//            "update_time text)";
//
//    //书库类别
//    private static final String STACK_ROOM_CATEGORY = "create table stack_room_category(_id integer primary key autoincrement," +
//            "cate_name text," +     //分类名
//            "vol_count integer," + //每类数量
//            "category_id integer)"; //分类id
//
//    //书库书名
//    private static final String STACK_ROOM_VOLUME = "create table stack_room_volume(_id integer primary key autoincrement," +
//            "vol_name text," +     //名字
//            "chp_count integer," + //每类数量
//            "update_time text," +
//            "version text," +//版本
//            "category_id integer)"; //分类id
//
//    //书库章节
//    private static final String STACK_ROOM_CHAPTER = "create table stack_room_chapter(_id integer primary key autoincrement," +
//            "index_id text," +
//            "name text," +
//            "volume_id integer," +
//            "content text," +
//            "version text," +
//            "category_id integer," +
//            "parent_id integer)";
//
//    //灵修类别
//    private static final String SPIRITUALITY_CATEGORY = "create table spirituality_category(_id integer primary key autoincrement," +
//            "cate_name text," +
//            "vol_count text," +
//            "parent_id integer," +
//            "update_time integer)";
//
//    //灵修
//    private static final String SPIRITUALITY = "create table spirituality(_id integer primary key autoincrement," +
//            "daytime text," +
//            "book text," +
//            "name text," +
//            "parent text," +
//            "content text)";
    private static final String DB_NAME = "yanjing.db";
    private static volatile DBHelper dbHelper = null;

    public DBHelper(Context context, int version) {
        super(context, DB_NAME, null, version);
    }


    public static DBHelper getDBHelper(Context context, int version) {
        if (dbHelper == null) {
            synchronized (DBHelper.class) {
                if (dbHelper == null) {
                    dbHelper = new DBHelper(context, version);
                }
            }
        }
        return dbHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

//        db.execSQL(TOTAL_CATALOG);
//        db.execSQL(SECONDARY_CATALOG);
//        db.execSQL(THIRD_LEVEL_CATALOG);
//        db.execSQL(BOOKS);
//        db.execSQL(CHAPTER);
//        db.execSQL(BOOK_CONTENT);


//        db.execSQL(BAIKE);
//        db.execSQL(BAIKE_CATEGORY);
//        db.execSQL(STACK_ROOM_CATEGORY);
//        db.execSQL(STACK_ROOM_VOLUME);
//        db.execSQL(STACK_ROOM_CHAPTER);
//        db.execSQL(SPIRITUALITY_CATEGORY);
//        db.execSQL(SPIRITUALITY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//        update(db, oldVersion, newVersion);
        //当版本号为2的时候 会走case1   新版本为3 旧版本为1时 会走case1和case2
        for (int i = oldVersion; i < newVersion; i++) {
            switch (i) {
                case 1:

                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    break;
            }
        }
//        if (newVersion == 2) {
//            db.execSQL("ALTER TABLE patient RENAME TO patient_temp");
//
//            db.execSQL("create table patient(_id integer primary key autoincrement," +
//                    "num integer," +
//                    "patient_name text," +
//                    "patient_sex text," +
//                    "bed_num text," +
//                    "hospital_num text," +
//                    "treatment_doctor text," +
//                    "wei_ji_zhi integer," +
//                    "hu_li_deng_ji integer," +
//                    "bing_wei_zhuang_tai integer," +
//                    "kang_jun_yao_guan_li integer," +
//                    "hui_zhen_zhuang_tai integer," +
//                    "drug_name text," +
//                    "sponsor_doctor text)");
//
//            db.execSQL("insert into patient(_id, num,patient_name,patient_sex," +
//                    "bed_num,hospital_num,treatment_doctor,wei_ji_zhi,hu_li_deng_ji," +
//                    "bing_wei_zhuang_tai,kang_jun_yao_guan_li,hui_zhen_zhuang_tai,drug_name," +
//                    "sponsor_doctor) "
//                    + "select _id,num,patient_name,patient_sex," +
//                    "bed_num,hospital_num,treatment_doctor,wei_ji_zhi,hu_li_deng_ji," +
//                    "bing_wei_zhuang_tai,kang_jun_yao_guan_li,hui_zhen_zhuang_tai,null,null from patient_temp");
//
//            db.execSQL("DROP TABLE patient_temp");
//
//        }
    }

    /**
     * 数据库版本递归更新
     *
     * @param oldVersion 数据库当前版本号
     * @param newVersion 数据库升级后的版本号
     * @retrun void
     */
//    public static void update(SQLiteDatabase db, int oldVersion, int newVersion) {
//        Upgrade upgrade = null;
//        if (oldVersion > newVersion) {
//            oldVersion++;
//            upgrade = VersionFactory.getUpgrade(oldVersion);
//            if (upgrade == null) {
//                return;
//            }
//            upgrade.update(db);
//            update(db, oldVersion, newVersion);
//        }
//    }

//    public class VersionSecond extends Upgrade {
//        @Override
//        public void update(SQLiteDatabase db) {
//            //数据库版本升级时会执行这个方法
//            //第一步将表A重命名为temp_A
//            //第二步创建新表A,此时表结构已加了2列
//            //第三步将temp_A表中的数据插入到表A
//            //第四步删除临时表temp_A</span>
//        }
//    }
}


/***************************************************表结构******************************************************/


