package com.sdattg.yanjing.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.adapter.AudioAdapter;
import com.sdattg.yanjing.adapter.VideoAdapter;
import com.sdattg.yanjing.base.BaseActivity;
import com.sdattg.yanjing.bean.AudioBean;
import com.sdattg.yanjing.bean.VideoBean;

import java.util.ArrayList;

import butterknife.Bind;

/**
 * 音频
 * Created by Administrator on 2019/7/24.
 */

public class AudioActivity extends BaseActivity {
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    private ArrayList<AudioBean> list = new ArrayList<>();
    private AudioAdapter audioAdapter;

    @Override
    public int getLayout() {
        return R.layout.activity_audio;
    }

    @Override
    public void initView() {

        whiteBack.setVisibility(View.GONE);
        setMiddleTitle("音频");

        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        for (int i = 0; i < 50; i++) {
            AudioBean audioBean = new AudioBean();
            audioBean.setImg(R.drawable.play);
            audioBean.setName("创世纪音频");
            audioBean.setUrl(R.drawable.picture);
            list.add(audioBean);
        }
        audioAdapter = new AudioAdapter(mContext);
        recyclerView.setAdapter(audioAdapter);
        audioAdapter.setAudio(list);
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {

    }
}
