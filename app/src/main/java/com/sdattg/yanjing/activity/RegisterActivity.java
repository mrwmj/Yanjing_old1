package com.sdattg.yanjing.activity;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.Utils.UIUtils;
import com.sdattg.yanjing.base.BaseActivity;
import com.sdattg.yanjing.config.ConfigKey;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 注册
 * Created by Administrator on 2019/8/22.
 */

public class RegisterActivity extends BaseActivity {
    @Bind(R.id.et_register_phone)
    EditText etRegisterPhone;
    @Bind(R.id.line_register_phone)
    View lineRegisterPhone;
    @Bind(R.id.et_register_code)
    EditText etRegisterCode;
    @Bind(R.id.tv_register_get_code)
    TextView tvRegisterGetCode;
    @Bind(R.id.line_register_code)
    View lineRegisterCode;
    @Bind(R.id.et_register_password)
    EditText etRegisterPassword;
    @Bind(R.id.line_register_password)
    View lineRegisterPassword;
    @Bind(R.id.tv_register)
    TextView tvRegister;
    @Bind(R.id.tv_protocol)
    TextView tvProtocol;


    private String vCode = "";
    @Override
    public int getLayout() {
        return R.layout.activity_register;
    }

    @Override
    public void initView() {
        setActionBarGone();
    }

    @Override
    public void initListener() {
        etRegisterCode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    lineRegisterCode.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                } else {
                    lineRegisterCode.setBackgroundColor(getResources().getColor(R.color.color_text_cf));
                }
            }
        });

        etRegisterPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    lineRegisterPassword.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                } else {
                    lineRegisterPassword.setBackgroundColor(getResources().getColor(R.color.color_text_cf));
                }
            }
        });
        etRegisterPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    lineRegisterPhone.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                } else {
                    lineRegisterPhone.setBackgroundColor(getResources().getColor(R.color.color_text_cf));
                }
            }
        });
    }

    @Override
    public void initData() {

    }


    @OnClick({R.id.tv_register_get_code, R.id.tv_register, R.id.tv_protocol})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_register_get_code://发送验证码
                sendCode();
                break;
            case R.id.tv_register://注册
                register();
                break;
            case R.id.tv_protocol://用户协议
                showProtocol();
                break;
        }
    }


    private void register() {
        String account = etRegisterPhone.getText().toString().trim();
        String code = etRegisterCode.getText().toString().trim();
        String password = etRegisterPassword.getText().toString().trim();

        if (account == null || account.equals("")) {
            UIUtils.showToast("请输入手机号");
            return;
        }
        if (code == null || code.equals("")) {
            UIUtils.showToast("请输入验证码");
            return;
        }
        if (password == null || password.equals("")) {
            UIUtils.showToast("请输入密码");
            return;
        }
        if (!UIUtils.checkMobile(account)) {
            UIUtils.showToast("其输入正确的手机号码");
            return;
        }
        if (!code.equals(vCode)) {
            UIUtils.showToast("验证码错误");
            return;
        }
        UIUtils.showToast("注册成功");
        login(account,password);
    }

    private void sendCode() {
        String account = etRegisterPhone.getText().toString().trim();
        if (account == null || account.equals("")) {
            UIUtils.showToast("请输入手机号");
            return;
        }
        if (!UIUtils.checkMobile(account)) {
            UIUtils.showToast("其输入正确的手机号码");
            return;
        }
        UIUtils.showToast("验证码发送成功");

        vCode = "123456";
    }

    private void showProtocol() {
        UIUtils.showToast("用户协议");
    }


    private void login(String account, String password) {
        mConfigUtils.put(ConfigKey.LOGIN_ACCOUNT, account);
        mConfigUtils.put(ConfigKey.PASSWORD, password);

        UIUtils.showToast("登录成功");

        Intent intent = new Intent();
        intent.putExtra("account",account);
        intent.putExtra("password",password);
        setResult(102,intent);
        finish();
    }
}
