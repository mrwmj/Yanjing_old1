package com.sdattg.yanjing.base;

import android.app.Application;
import android.content.Context;

import com.sdattg.yanjing.db.DatabaseUtil;

import org.xutils.x;

/**
 * Created by Administrator on 2019/7/10.
 */

public class BaseApplication extends Application {
    private static Context context;

//    private DbManager.DaoConfig daoConfig;
//
//    public DbManager.DaoConfig getDaoConfig() {
//        return daoConfig;
//    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        DatabaseUtil.packDataBase(context);
        x.Ext.init(this);
        x.Ext.setDebug(true);
    }

    public static Context getContext() {
        return context;
    }
}
