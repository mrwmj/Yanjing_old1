package com.sdattg.yanjing.bean.zuji;

import org.xutils.db.annotation.Column;
import org.xutils.db.annotation.Table;

/**
 * 书签
 * Created by Administrator on 2019/8/22.
 */

@Table(name = "ShuQian")
public class ShuQianBean {
    //不可以少
    @Column(name = "id", isId = true)
    public int id;

    @Column(name = "book_name")
    public String book_name;//书名

    @Column(name = "chapter_name")
    public String chapter_name;//章节

    @Column(name = "collection_content")
    public String collection_content;//收藏内容

    @Column(name = "catalog_name")
    public String catalog_name;//目录名

    @Column(name = "leave_comments")
    public String leave_comments;//留言

    @Column(name = "time")
    public String time;//时间

    @Column(name = "open")
    public boolean open;

}
