package com.sdattg.yanjing.bean;

/**
 * Created by Administrator on 2019/7/26.
 */

public class BooksBean {
    private String books_name;//书名
    private String books_min_name;//缩略书名
    private String books_version;//版本
    private String books_summary;//书籍简介
    private String books_id;//书籍id
    private String catalog_rank;//所属目录层级
    private String catalog_id;//所属目录id

    public String getBooks_name() {
        return books_name;
    }

    public void setBooks_name(String books_name) {
        this.books_name = books_name;
    }

    public String getBooks_min_name() {
        return books_min_name;
    }

    public void setBooks_min_name(String books_min_name) {
        this.books_min_name = books_min_name;
    }

    public String getBooks_version() {
        return books_version;
    }

    public void setBooks_version(String books_version) {
        this.books_version = books_version;
    }

    public String getBooks_summary() {
        return books_summary;
    }

    public void setBooks_summary(String books_summary) {
        this.books_summary = books_summary;
    }

    public String getBooks_id() {
        return books_id;
    }

    public void setBooks_id(String books_id) {
        this.books_id = books_id;
    }

    public String getCatalog_rank() {
        return catalog_rank;
    }

    public void setCatalog_rank(String catalog_rank) {
        this.catalog_rank = catalog_rank;
    }

    public String getCatalog_id() {
        return catalog_id;
    }

    public void setCatalog_id(String catalog_id) {
        this.catalog_id = catalog_id;
    }

    @Override
    public String toString() {
        return "BooksBean{" +
                "books_name='" + books_name + '\'' +
                ", books_min_name='" + books_min_name + '\'' +
                ", books_version='" + books_version + '\'' +
                ", books_summary='" + books_summary + '\'' +
                ", books_id='" + books_id + '\'' +
                ", catalog_rank='" + catalog_rank + '\'' +
                ", catalog_id='" + catalog_id + '\'' +
                '}';
    }
}
