package com.sdattg.yanjing.Utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.sdattg.yanjing.base.BaseApplication;
import com.xuezj.cardbanner.imageloader.CardImageLoader;

/**
 * Created by Administrator on 2019/7/24.
 */

public class GlideImageLoader2 implements CardImageLoader {
    public void load(Context context, ImageView imageView, Object path) {
//        Log.i("GlideImageLoader2", "load: "+imageView.getWidth()+"            "+imageView.getHeight());
        Glide.with(BaseApplication.getContext()).load(path).into(imageView);
//        x.image().bind(imageView, (String) path);
//        MCUtils.fillImage();
//        Utils.fillImage(imageView, (String) path);
    }
}