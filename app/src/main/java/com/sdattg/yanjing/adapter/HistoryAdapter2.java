package com.sdattg.yanjing.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.bean.zuji.HistoryRecordBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2019/7/26.
 */

public class HistoryAdapter2 extends BaseAdapter {
    private static String TAG = "HistoryAdapter";

    private List<HistoryRecordBean> beans = new ArrayList<>();
    private Context mContext;
    private Map<Integer, Boolean> map = new HashMap<>();

    public HistoryAdapter2(Context mContext) {
        this.mContext = mContext;
    }

    public void setData(List<HistoryRecordBean> beans) {
        this.beans.clear();
        this.beans.addAll(beans);
        notifyDataSetChanged();
    }

    public void addData(List<HistoryRecordBean> beans) {
        this.beans.addAll(beans);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return beans.size();
    }

    @Override
    public HistoryRecordBean getItem(int position) {
        return beans.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        HistoryRecordBean historyBean = beans.get(position);
        HistoryHolder holder = null;
        if (convertView == null) {
            convertView = View.inflate(mContext,
                    R.layout.adapter_history_item, null);
            holder =new HistoryHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (HistoryHolder) convertView.getTag();
        }

        holder.tvBooksMinName.setText(historyBean.min_name);
        holder.tvBooksName.setText(historyBean.book_name);
        holder.tvTime.setText(historyBean.time);
        holder.tvBooksInfo.setText(historyBean.chapter_name);

        //是否显示置顶
        if (historyBean.is_collect == 0) {
            holder.ivZhiding.setVisibility(View.VISIBLE);
        } else {
            holder.ivZhiding.setVisibility(View.GONE);
        }

        //显示清空所有
        if (map.size() == beans.size()) {
            if (map.get(position)) {
                holder.checkBox.setChecked(true);
            } else {
                holder.checkBox.setChecked(false);
            }
            holder.checkBox.setVisibility(View.VISIBLE);
        } else {
            holder.checkBox.setVisibility(View.GONE);
        }


        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (map.size() == beans.size()) {
                    map.put(position, isChecked);
                }
            }
        });
        return convertView;
    }


    public void initChebox(int itemCount) {
        for (int i = 0; i < itemCount; i++) {
            map.put(i, true);
        }
    }

    public void clearChebox() {
        map.clear();

    }

    public Map<Integer, Boolean> getCheboxMap() {
        return map;
    }

    public HistoryRecordBean getDeleteBean(int position) {
        return beans.get(position);
    }

    public class HistoryHolder {
        @Bind(R.id.tv_books_min_name)
        TextView tvBooksMinName;
        @Bind(R.id.tv_books_name)
        TextView tvBooksName;
        @Bind(R.id.tv_time)
        TextView tvTime;
        @Bind(R.id.tv_books_info)
        TextView tvBooksInfo;
        @Bind(R.id.iv_zhi_ding)
        ImageView ivZhiding;
        @Bind(R.id.check_box)
        CheckBox checkBox;

        public HistoryHolder(View itemView) {
            ButterKnife.bind(this, itemView);
            itemView.setTag(this);
        }
    }

//    @NonNull
//    @Override
//    public HistoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View inflate = LayoutInflater.from(mContext).inflate(R.layout.adapter_history_item, parent, false);
//        HistoryHolder videoHolder = new HistoryHolder(inflate);
//        return videoHolder;
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull HistoryHolder holder, final int position) {
//        final HistoryRecordBean bean = beans.get(position);
//        holder.tvBooksMinName.setText(bean.min_name);
//        holder.tvBooksName.setText(bean.book_name);
//        holder.tvTime.setText(bean.time);
//        holder.tvBooksInfo.setText(bean.chapter_name);
//
//        //是否显示置顶
//        if (bean.is_collect == 0) {
//            holder.ivZhiding.setVisibility(View.VISIBLE);
//        } else {
//            holder.ivZhiding.setVisibility(View.GONE);
//        }
//
//        //显示清空所有
//        if (map.size() == beans.size()) {
//            if (map.get(position)) {
//                holder.checkBox.setChecked(true);
//            } else {
//                holder.checkBox.setChecked(false);
//            }
//            holder.checkBox.setVisibility(View.VISIBLE);
//        } else {
//            holder.checkBox.setVisibility(View.GONE);
//        }
//
//
//        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (map.size() == beans.size()) {
//                    map.put(position, isChecked);
//                }
//            }
//        });
//
//
//    }
//
//    @Override
//    public int getItemCount() {
//        return beans.size();
//    }


}

