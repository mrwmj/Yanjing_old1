package com.sdattg.yanjing.bean;

/**
 * Created by Administrator on 2019/7/29.
 */

public class AudioBean {
    private int url;
    private String name;
    private int img;

    public int getUrl() {
        return url;
    }

    public void setUrl(int url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    @Override
    public String toString() {
        return "AudioBean{" +
                "url=" + url +
                ", name='" + name + '\'' +
                ", img=" + img +
                '}';
    }
}
