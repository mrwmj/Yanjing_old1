package com.sdattg.yanjing.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.Utils.UIUtils;
import com.sdattg.yanjing.adapter.FmPagerAdapter;
import com.sdattg.yanjing.fragment.books.ShengJingFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * 搜索
 * Created by Administrator on 2019/7/24.
 */

public class SearchActivity extends AppCompatActivity {
    @Bind(R.id.iv_user_white)
    ImageView ivUserWhite;
    @Bind(R.id.title_name)
    TextView titleName;
    @Bind(R.id.iv_right_icon)
    ImageView ivRightIcon;
    @Bind(R.id.title_root_view)
    RelativeLayout titleRootView;
    @Bind(R.id.et_search)
    EditText etSearch;
    @Bind(R.id.tv_search)
    TextView tvSearch;
    @Bind(R.id.ll_search)
    LinearLayout llSearch;

    @Bind(R.id.spinner)
    Spinner spinner;
    @Bind(R.id.spinner2)
    Spinner spinner2;
    @Bind(R.id.tab_layout)
    TabLayout tabLayout;
    @Bind(R.id.view_pager)
    ViewPager viewPager;

    private ArrayAdapter<String> arrayAdapter;
    private ArrayAdapter<String> arrayAdapter2;

    private ArrayList<String> titles = new ArrayList<>();
    private ArrayList<Fragment> fragments = new ArrayList<>();
    private FmPagerAdapter pagerAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        initView();
        initListener();
        initData();
    }

    private void initView() {

        titles.add("圣经");
        titles.add("怀著");
        titles.add("其他");
        fragments.add(new ShengJingFragment("1"));
        fragments.add(new ShengJingFragment("1"));
        fragments.add(new ShengJingFragment("1"));

        for (int i = 0; i < titles.size(); i++) {
            tabLayout.addTab(tabLayout.newTab().setText(titles.get(i)));
        }

        tabLayout.setupWithViewPager(viewPager);
        pagerAdapter = new FmPagerAdapter(getSupportFragmentManager(), titles, fragments);
        viewPager.setAdapter(pagerAdapter);

    }

    private void initListener() {
        ivUserWhite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ivRightIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UIUtils.showToast("开发中，请等待");
            }
        });

    }

    private void initData() {
        initList();
        initList2();
        arrayAdapter = new ArrayAdapter<String>(SearchActivity.this, android.R.layout.simple_spinner_item, teamList);

        //设置下拉列表的风格
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //将adapter 添加到spinner中
        spinner.setAdapter(arrayAdapter);
        //设置点击事件
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                textView.setText(teamList.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        arrayAdapter2 = new ArrayAdapter<String>(SearchActivity.this, android.R.layout.simple_spinner_item, teamList2);

        //设置下拉列表的风格
        arrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //将adapter 添加到spinner中
        spinner2.setAdapter(arrayAdapter2);
        //设置点击事件
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                textView.setText(teamList.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

    }

    private List<String> teamList;
    private List<String> teamList2;

    public void initList() {
        teamList = new ArrayList<>();
        teamList.add("版本");
        teamList.add("作者");
        teamList.add("灵修");
        teamList.add("怀著");
        teamList.add("书库");
        teamList.add("其他");
    }

    public void initList2() {
        teamList2 = new ArrayList<>();
        teamList2.add("注释");
        teamList2.add("注释");
        teamList2.add("注释");
        teamList2.add("注释");
        teamList2.add("注释");
        teamList2.add("注释");
    }
}
