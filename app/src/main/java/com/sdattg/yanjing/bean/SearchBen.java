package com.sdattg.yanjing.bean;

/**
 * Created by Administrator on 2019/8/9.
 */

public class SearchBen {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
