package com.sdattg.yanjing.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sdattg.yanjing.R;
import com.sdattg.yanjing.bean.AudioBean;
import com.sdattg.yanjing.bean.AudioBean;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2019/7/26.
 */

public class AudioAdapter extends RecyclerView.Adapter<AudioAdapter.AudioHolder> {
    private static String TAG = "AudioAdapter";

    private ArrayList<AudioBean> audioBeans = new ArrayList<>();
    private Context mContext;

    public AudioAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setAudio(ArrayList<AudioBean> audioBeans) {
        this.audioBeans.clear();
        this.audioBeans.addAll(audioBeans);
        notifyDataSetChanged();
    }

    public void addAudio(ArrayList<AudioBean> AudioBeans) {
        this.audioBeans.addAll(AudioBeans);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AudioHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(mContext).inflate(R.layout.adapter_audio_item, parent, false);
        AudioHolder audioHolder = new AudioHolder(inflate);
        return audioHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AudioHolder holder, int position) {
        AudioBean audioBean = audioBeans.get(position);


        Glide.with(mContext).load(audioBean.getUrl()).into(holder.ivUrl);
        Glide.with(mContext).load(audioBean.getImg()).into(holder.ivPlay);

        holder.tvName.setText(audioBean.getName());
    }

    @Override
    public int getItemCount() {
        return audioBeans.size();
    }


    public class AudioHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.iv_url)
        ImageView ivUrl;
        @Bind(R.id.tv_name)
        TextView tvName;
        @Bind(R.id.iv_play)
        ImageView ivPlay;
        public AudioHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

