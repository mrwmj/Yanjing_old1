package com.sdattg.yanjing.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.bean.BooksBean;
import com.sdattg.yanjing.config.ConfigKey;
import com.sdattg.yanjing.config.ConfigUtils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2019/8/12.
 */

public class SJXinYueJiuGongGeAdapter extends RecyclerView.Adapter<SJXinYueJiuGongGeAdapter.SJXinYueHolder> {
    private static String TAG = "SJXinYueLieBiaoAdapter";

    private ArrayList<BooksBean> books = new ArrayList<>();
    private Context mContext;
    protected ConfigUtils mConfigUtils;

    public SJXinYueJiuGongGeAdapter(Context mContext) {
        this.mContext = mContext;
        mConfigUtils = new ConfigUtils(mContext);
    }

    public void setBooks(ArrayList<BooksBean> books) {
        this.books.clear();
        this.books.addAll(books);
        notifyDataSetChanged();
    }

    public void addBooks(ArrayList<BooksBean> books) {
        this.books.addAll(books);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SJXinYueHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View  inflate = LayoutInflater.from(mContext).inflate(R.layout.adapter_books_item_jiu_gong_ge, parent, false);
        SJXinYueHolder SJXinYueHolder = new SJXinYueHolder(inflate);
        return SJXinYueHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SJXinYueHolder holder, int position) {
        BooksBean booksBean = books.get(position);
        String booksName = booksBean.getBooks_name();

        String[] names = booksName.split("\\[|\\]");
        if (names.length > 2) {
            /**
             * 包含[]
             */

            //判断是否包含{}
            String[] minNames = names[0].split("\\{|\\}");
            if (minNames.length > 2) {
                if (TextUtils.isEmpty(minNames[1])) {
                    holder.tvJggBookMinName.setText(names[2].substring(0, 1));
                } else {
                    holder.tvJggBookMinName.setText(minNames[1]);
                }
                if (minNames[2].contains("(")) {
                    holder.tvJggBooksName.setText(minNames[2].substring(0, minNames[2].indexOf("(")));
                } else {
                    holder.tvJggBooksName.setText(minNames[2]);
                }

            } else {
                //不包含{}包含[]
                holder.tvJggBookMinName.setText(names[2].substring(0, 1));

                if (names[2].contains("(")) {
                    holder.tvJggBooksName.setText(names[2].substring(0, names[2].indexOf("(")));
                } else {
                    holder.tvJggBooksName.setText(names[2]);
                }
            }
        } else {
            /**
             * 不包含[]
             */
            //判断是否包含{}
            String[] minNames = names[0].split("\\{|\\}");
            if (minNames.length > 2) {
                if (TextUtils.isEmpty(minNames[1])) {
                    holder.tvJggBookMinName.setText(minNames[2].substring(0, 1));
                } else {
                    holder.tvJggBookMinName.setText(minNames[1]);
                }

                if (minNames[2].contains("(")) {
                    holder.tvJggBooksName.setText(minNames[2].substring(0, minNames[2].indexOf("(")));
                } else {
                    holder.tvJggBooksName.setText(minNames[2]);
                }
            } else {
                //不包含{}也不包含[]
                holder.tvJggBookMinName.setText(minNames[0].substring(0, 1));
                if (booksName.contains("(")) {
                    holder.tvJggBooksName.setText(booksName.substring(0, booksName.indexOf("(")));
                } else {
                    holder.tvJggBooksName.setText(booksName);
                }
            }

        }
    }

    @Override
    public int getItemCount() {
        return books.size();
    }


    public class SJXinYueHolder extends RecyclerView.ViewHolder {


        @Bind(R.id.tv_jgg_books_min_name)
        TextView tvJggBookMinName;
        @Bind(R.id.tv_jgg_books_name)
        TextView tvJggBooksName;

        public SJXinYueHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}