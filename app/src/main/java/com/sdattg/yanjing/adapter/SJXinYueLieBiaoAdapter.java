package com.sdattg.yanjing.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.bean.BooksBean;
import com.sdattg.yanjing.config.ConfigKey;
import com.sdattg.yanjing.config.ConfigUtils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2019/8/12.
 */

public class SJXinYueLieBiaoAdapter extends RecyclerView.Adapter<SJXinYueLieBiaoAdapter.SJXinYueHolder> {
    private static String TAG = "SJXinYueLieBiaoAdapter";

    private ArrayList<BooksBean> books = new ArrayList<>();
    private Context mContext;
    protected ConfigUtils mConfigUtils;

    public SJXinYueLieBiaoAdapter(Context mContext) {
        this.mContext = mContext;
        mConfigUtils = new ConfigUtils(mContext);
    }

    public void setBooks(ArrayList<BooksBean> books) {
        this.books.clear();
        this.books.addAll(books);
        notifyDataSetChanged();
    }

    public void addBooks(ArrayList<BooksBean> books) {
        this.books.addAll(books);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SJXinYueHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        String string = mConfigUtils.getString(ConfigKey.SHOW_STYLE, "LinearLayout");
        View inflate = LayoutInflater.from(mContext).inflate(R.layout.adapter_books_item_lie_biao, parent, false);
        SJXinYueHolder SJXinYueHolder = new SJXinYueHolder(inflate);
        return SJXinYueHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SJXinYueHolder holder, int position) {
        BooksBean booksBean = books.get(position);
        String booksName = booksBean.getBooks_name();

        String[] names = booksName.split("\\[|\\]");
        if (names.length > 2) {
            /**
             * 包含[]
             */

            //判断是否包含{}
            String[] minNames = names[0].split("\\{|\\}");
            if (minNames.length > 2) {
                if (TextUtils.isEmpty(minNames[1])) {
                    holder.tvBooksMinName.setText(names[2].substring(0, 1));
                } else {
                    holder.tvBooksMinName.setText(minNames[1]);
                }
                if (minNames[2].contains("(")) {
                    holder.tvBooksTime.setText(minNames[2].substring(minNames[2].indexOf("("), minNames[2].length()));
                    holder.tvBooksName.setText(minNames[2].substring(0, minNames[2].indexOf("(")));
                } else {
                    holder.tvBooksTime.setText("");
                    holder.tvBooksName.setText(minNames[2]);
                }

            } else {
                //不包含{}包含[]
                holder.tvBooksMinName.setText(names[2].substring(0, 1));

                if (names[2].contains("(")) {
                    holder.tvBooksTime.setText(names[2].substring(names[2].indexOf("("), names[2].length()));
                    holder.tvBooksName.setText(names[2].substring(0, names[2].indexOf("(")));
                } else {
                    holder.tvBooksTime.setText("");
                    holder.tvBooksName.setText(names[2]);
                }
            }
        } else {
            /**
             * 不包含[]
             */
            //判断是否包含{}
            String[] minNames = names[0].split("\\{|\\}");
            if (minNames.length > 2) {
                if (TextUtils.isEmpty(minNames[1])) {
                    holder.tvBooksMinName.setText(minNames[2].substring(0, 1));
                } else {
                    holder.tvBooksMinName.setText(minNames[1]);
                }

                if (minNames[2].contains("(")) {
                    holder.tvBooksTime.setText(minNames[2].substring(minNames[2].indexOf("("), minNames[2].length()));
                    holder.tvBooksName.setText(minNames[2].substring(0, minNames[2].indexOf("(")));
                } else {
                    holder.tvBooksTime.setText("");
                    holder.tvBooksName.setText(minNames[2]);
                }
            } else {
                //不包含{}也不包含[]
                holder.tvBooksMinName.setText(minNames[0].substring(0, 1));
                if (booksName.contains("(")) {
                    holder.tvBooksTime.setText(booksName.substring(booksName.indexOf("("), booksName.length()));
                    holder.tvBooksName.setText(booksName.substring(0, booksName.indexOf("(")));
                } else {
                    holder.tvBooksTime.setText("");
                    holder.tvBooksName.setText(booksName);
                }
            }

        }


//        holder.tvBooksInfo.setText(booksBean.getBooks_summary());
//        holder.tvBooksInfo.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return books.size();
    }


    public class SJXinYueHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_books_time)
        TextView tvBooksTime;
        @Bind(R.id.tv_books_min_name)
        TextView tvBooksMinName;
        @Bind(R.id.tv_books_name)
        TextView tvBooksName;
        @Bind(R.id.tv_books_info)
        TextView tvBooksInfo;
        public SJXinYueHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}