package com.sdattg.yanjing.interfaces;

public interface NetProgressCallBack{

	/**
	 * 请求成功的回调
	 * @param data 数据
	 * @param requestID 请求id，要保证唯一性
	 */
	void onRequestSuccess(Object data, String requestID);

	void onProgress(float progress, long total);

	/**
	 * 请求失败的回调
	 * @param requestID 请求id，要保证唯一性
	 */
	void onRequestFailure(String requestID);
}
