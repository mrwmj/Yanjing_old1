package com.sdattg.yanjing.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.Utils.UIUtils;
import com.sdattg.yanjing.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by Administrator on 2019/7/24.
 */

public class LingXiuFragment extends BaseFragment {
    @Bind(R.id.tv_ri_qi)
    TextView tvRiQi;
//    @Bind(R.id.iv_ri_qi)
//    ImageView ivRiQi;
    @Bind(R.id.tv_zhou)
    TextView tvZhou;
    @Bind(R.id.view_pager)
    ViewPager viewPager;

    private List<BenDiFragment> list = new ArrayList<>();
    private List<String> lists = new ArrayList<>();

    @Override
    public int getLayout() {
        return R.layout.fragment_ling_xiu;


    }

    @Override
    public void initView() {
        String dateStr = UIUtils.getDateStr(System.currentTimeMillis());
        int week = UIUtils.whatWeek(System.currentTimeMillis());
        tvRiQi.setText(dateStr);
        tvZhou.setText("第" + week + "周");
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {
        MyPagerAdapter pagerAdapter = new MyPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.i("onPageSelected", "onPageScrollStateChanged: " + position);
                tvRiQi.setText(position + "");
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }

    public class MyPagerAdapter extends FragmentPagerAdapter {


        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
            list.clear();
            for (int i = 0; i < 365; i++) {
                list.add(new BenDiFragment());
            }
        }

        @Override
        public Fragment getItem(int position) {
            return list.get(position);
        }

        @Override
        public int getCount() {
            return list.size();
        }
    }
}
