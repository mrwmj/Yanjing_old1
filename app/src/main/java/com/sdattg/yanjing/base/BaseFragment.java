package com.sdattg.yanjing.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.sdattg.yanjing.Utils.DbUtils;
import com.sdattg.yanjing.config.ConfigUtils;
import com.sdattg.yanjing.db.DBManager;
import com.sdattg.yanjing.interfaces.UiInterface;

import org.xutils.DbManager;

import butterknife.ButterKnife;


/**
 * Description:整个项目的所有fragment的基类
 */
public abstract class BaseFragment extends Fragment implements UiInterface {

    private View mView;
    public Activity mActivity;
    //    private ProgressUtils progressUtils;
//    protected NetKit mNetKit;
    protected ConfigUtils mConfigUtils;
    protected DBManager dbManager;
    protected DbManager db;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
//        progressUtils = new ProgressUtils(mActivity);
        mConfigUtils = new ConfigUtils(mActivity);
        dbManager = new DBManager(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(getLayout(), container, false);
        ButterKnife.bind(this, mView);
//        mNetKit = NetKit.getInstance();
        db = DbUtils.getDB();
        initView();
        initListener();
        initData();
        return mView;
    }

    /**
     * 显示进度条
     */
//    public void showProgress() {
//        if (NetUtil.checkNet(mActivity)) {
//            progressUtils.showProgressDialog();
//        }
//    }

    /**
     * 关闭进度条
     */
//    public void closeProgress() {
//        if (NetUtil.checkNet(mActivity)) {
//            progressUtils.closeProgressDialog();
//        }
//    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

}
