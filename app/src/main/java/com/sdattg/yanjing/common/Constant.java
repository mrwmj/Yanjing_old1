package com.sdattg.yanjing.common;

public class Constant {

    /**
     * 列表模式
     */
    public static final String LIE_BIAO = "LinearLayout";
    /**
     * 九宫格模式
     */
    public static final String JIU_GONG_GE = "GridLayout";


}
