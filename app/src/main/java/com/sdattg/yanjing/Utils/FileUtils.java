package com.sdattg.yanjing.Utils;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import org.xutils.x;

import java.io.File;
import java.math.BigDecimal;

public class FileUtils {
    public static final String CACHE = "cache";//数据以及其他东西的缓存目录
    public static final String DOWNLOAD = "download";//下载目录
    public static final String ICON = "icon";//图标缓存目录
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    public static final String ROOT = "YanJing";
    public static final String ROOT1 = "medownload";


    //获取存储文件的存储对象
    public static File getIconDir() {
        return getDir(ICON);
    }
    //获取下载目录
    public static File getDownloadDir() {
        return getDir(DOWNLOAD);
    }
    //通过文件路径来获取缓存文件
    public static File getCacheDir() {
        return getDir(CACHE);
    }

    //通过路径来获取缓存文件
    public static File getDir(String cache) {
        StringBuilder path = new StringBuilder();
        if (isSDAvailable()) {
            path.append(Environment.getExternalStorageDirectory().getAbsolutePath());
            path.append(File.separator);
            path.append("YanJing");
            path.append(File.separator);
            path.append(ROOT1);
            path.append(File.separator);
            path.append(cache);
        } else {
            path.append(x.app().getCacheDir().getAbsolutePath());
            path.append(File.separator);
            path.append(cache);
        }
        File file = new File(path.toString());
        if (!(file.exists() && file.isDirectory())) {
            file.mkdirs();
        }
        return file;
    }

//    public static File getDir_de() {
//        StringBuilder path = new StringBuilder();
//        if (isSDAvailable()) {
//            path.append(Environment.getExternalStorageDirectory().getAbsolutePath());
//            path.append(File.separator);
//            path.append("HWAPP");
//            path.append(File.separator);
//            path.append(ROOT1);
//        } else {
//            path.append(x.app().getCacheDir().getAbsolutePath());
//        }
//        File file = new File(path.toString());
//        if (!(file.exists() && file.isDirectory())) {
//            file.mkdirs();
//        }
//        return file;
//    }
    //获取当前应用的缓存文件路径
    public static File getDir_de1() {
        StringBuilder path = new StringBuilder();
        if (isSDAvailable()) {
            path.append(Environment.getExternalStorageDirectory().getAbsolutePath());
            path.append(File.separator);
            path.append("yanjing");
        } else {
            path.append(x.app().getCacheDir().getAbsolutePath());
        }
        File file = new File(path.toString());
        if (!(file.exists() && file.isDirectory())) {
            file.mkdirs();
        }
        return file;
    }

    //判断当前手机是否有外部存储
    public static boolean isSDAvailable() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return true;
        }
        return false;
    }

    //清除本应用下的所有数据
    public static void cleanApplicationData(Context context, String... filepath) {
        cleanInternalCache(context);
        cleanExternalCache(context);
        cleanDatabases(context);
        cleanSharedPreference(context);
        cleanFiles(context);
        if (filepath != null) {
            for (String filePath : filepath) {
                cleanCustomCache(filePath);
            }
        }
    }

    //根据传递的路径来删除相应的目录
    public static void cleanCustomCache(String filePath) {
        deleteFilesByDirectory(new File(filePath));
    }

    //删除外部存储中的缓存文件
    public static void cleanExternalCache(Context context) {
        if (Environment.getExternalStorageState().equals("mounted")) {
            deleteFilesByDirectory(context.getExternalCacheDir());
        }
    }
    //通过Context来清理文件
    public static void cleanFiles(Context context) {
        deleteFilesByDirectory(context.getFilesDir());
    }
    //清除网络下载的临时缓存或者通过网络请求的数据
    public static void cleanInternalCache(Context context) {
        deleteFilesByDirectory(context.getCacheDir());
    }

    //清理当前数据库
    public static void cleanDatabases(Context context) {
        deleteFilesByDirectory(new File("/data/data/" + context.getPackageName() + "/databases"));
    }

    //清理SharePreference中的文件
    public static void cleanSharedPreference(Context context) {
        deleteFilesByDirectory(new File("/data/data/" + context.getPackageName() + "/shared_prefs"));
    }
    //通过当前文件的目录来进行删除操作
    private static void deleteFilesByDirectory(File directory) {
        if (directory != null && directory.exists() && directory.isDirectory()) {
            for (File item : directory.listFiles()) {
                item.delete();
            }
        }
    }

    //通过文献路径来删除文件夹
    public static void deleteFolderFile(String filePath, boolean deleteThisPath, TextView a, View b) {
        if (!TextUtils.isEmpty(filePath)) {
            try {
                File file = new File(filePath);
                if (file.isDirectory()) {
                    File[] files = file.listFiles();
                    for (File absolutePath : files) {
                        deleteFolderFile(absolutePath.getAbsolutePath(), true, a, b);
                    }
                }
                if (!deleteThisPath) {
                    return;
                }
                if (!file.isDirectory()) {
                    file.delete();
                    a.setText("清理完成!");
                    b.setVisibility(View.VISIBLE);
                } else if (file.listFiles().length == 0) {
                    file.delete();
                    a.setText("清理完成!");
                    b.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //获取当前文件夹的大小（也就是当前文件夹中文件的整体大小）
    public static long getFolderSize(File file) throws Exception {
        long size = 0;
        try {
            File[] fileList = file.listFiles();
            for (int i = 0; i < fileList.length; i++) {
                long folderSize;
                if (fileList[i].isDirectory()) {
                    folderSize = getFolderSize(fileList[i]);
                } else {
                    folderSize = fileList[i].length();
                }
                size += folderSize;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return size;
    }

    //格式化当前文档或者是文件大小字符表现形式
    public static String getFormatSize(double size) {
        double kiloByte = size / 1024.0d;
        if (kiloByte < 1.0d) {
            return size + "Byte";
        }
        double megaByte = kiloByte / 1024.0d;
        if (megaByte < 1.0d) {
            return new BigDecimal(Double.toString(kiloByte)).setScale(2, 4).toPlainString() + "KB";
        }
        double gigaByte = megaByte / 1024.0d;
        if (gigaByte < 1.0d) {
            return new BigDecimal(Double.toString(megaByte)).setScale(2, 4).toPlainString() + "MB";
        }
        double teraBytes = gigaByte / 1024.0d;
        if (teraBytes < 1.0d) {
            return new BigDecimal(Double.toString(gigaByte)).setScale(2, 4).toPlainString() + "GB";
        }
        return new BigDecimal(teraBytes).setScale(2, 4).toPlainString() + "TB";
    }

    //获取缓存文件的大小
    public static String getCacheSize(File file) throws Exception {
        return getFormatSize((double) getFolderSize(file));
    }
}
