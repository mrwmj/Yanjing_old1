package com.sdattg.yanjing.interfaces;

public interface NetCallBack {

	/**
	 * 请求成功的回调
	 * @param data 数据
	 * @param requestID 请求id，要保证唯一性
	 */
	void onRequestSuccess(Object data, String requestID);

	/**
	 * 请求失败的回调
	 * @param requestID 请求id，要保证唯一性
	 */
	void onRequestFailure(String requestID);
}
