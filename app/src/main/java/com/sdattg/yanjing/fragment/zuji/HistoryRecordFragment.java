package com.sdattg.yanjing.fragment.zuji;

import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.Utils.UIUtils;
import com.sdattg.yanjing.activity.FootprintActivity;
import com.sdattg.yanjing.adapter.HistoryAdapter;
import com.sdattg.yanjing.base.BaseFragment;
import com.sdattg.yanjing.bean.zuji.HistoryRecordBean;
import com.sdattg.yanjing.dialog.ConfirmDialog;
import com.sdattg.yanjing.view.SlideRecyclerView;

import org.xutils.ex.DbException;

import java.util.List;
import java.util.Map;

import butterknife.Bind;

/**
 * 历史记录
 * Created by Administrator on 2019/7/29.
 */

public class HistoryRecordFragment extends BaseFragment {
    private static String TAG = "HistoryRecordFragment";
    @Bind(R.id.recycler_view)
    SlideRecyclerView recyclerView;

    @Bind(R.id.btn_add_data)
    Button addData;
    @Bind(R.id.ll_delete)
    LinearLayout llDelete;
    @Bind(R.id.tv_delete)
    TextView tvDelete;
    @Bind(R.id.tv_cancel)
    TextView tvCancel;

    private List<HistoryRecordBean> list = null;
    private HistoryAdapter historyAdapter;
    private FootprintActivity activity;

    public boolean is_show_delete = false;

    private ConfirmDialog confirmDialog;

    @Override
    public int getLayout() {
        return R.layout.fragment_history_record;
    }

    @Override
    public void initView() {
        activity = (FootprintActivity) getActivity();
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext().getApplicationContext()));

        historyAdapter = new HistoryAdapter(getContext(), this, recyclerView);
    }

    @Override
    public void initListener() {

        tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDialog = new ConfirmDialog.Builder()
                        .setTtile("提示")
                        .setContent("你确定要删除所有“历史记录”吗？不可恢复！")
                        .setOnCancleListener(onCancleListener)
                        .setOnConfirmListener(onConfirmListener)
                        .show(getActivity(), getFragmentManager());
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                is_show_delete = false;
                if (historyAdapter != null) {
                    historyAdapter.clearChebox();
                    historyAdapter.notifyDataSetChanged();
                }
                llDelete.setVisibility(View.GONE);
                addData.setVisibility(View.VISIBLE);
                activity.setClearAllText("清空所有");
            }
        });

        // 必须 最后执行
        recyclerView.setAdapter(historyAdapter);

        addData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HistoryRecordBean recordBean = new HistoryRecordBean();
                recordBean.min_name = "创";
                recordBean.book_name = "创世纪";
                recordBean.chapter_name = "第一章 创世纪创世纪";
                recordBean.time = UIUtils.getDateStr2(System.currentTimeMillis());
                recordBean.is_collect = 1;
                try {
                    db.save(recordBean);
                    initData();
                } catch (DbException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private View.OnClickListener onCancleListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (confirmDialog != null) {
                confirmDialog.dismiss();
            }
        }
    };
    private View.OnClickListener onConfirmListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (confirmDialog != null) {
                confirmDialog.dismiss();
                if (historyAdapter != null) {
                    Map<Integer, Boolean> cheboxMap = historyAdapter.getCheboxMap();
                    for (int key : cheboxMap.keySet()) {
                        if (cheboxMap.get(key)) {
                            HistoryRecordBean deleteBean = historyAdapter.getDeleteBean(key);
                            try {
                                db.delete(deleteBean);
                            } catch (DbException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    UIUtils.showToast("删除成功");
                    activity.setClearAllText("清空所有");
                    quanXuan(false);
                    initData();
                }
            }
        }
    };

    @Override
    public void initData() {
        try {
            list = db.selector(HistoryRecordBean.class)
//                    .orderBy("id", true)
                    .orderBy("is_collect", false).findAll();
//            list = db.findAll(HistoryRecordBean.class);
            if (list != null) {
                activity.upTabBadge(0, list.size());
                historyAdapter.setData(list);
                historyAdapter.clearChebox();
            }
        } catch (DbException e) {
            e.printStackTrace();
        }
    }


    /**
     * 置顶
     *
     * @param recordBean
     */
    public void topping(HistoryRecordBean recordBean) {
        recordBean.is_collect = 0;
        try {
            db.saveOrUpdate(recordBean);
//            list = db.findAll(HistoryRecordBean.class);
            list = db.selector(HistoryRecordBean.class)
//                    .orderBy("id", true)
                    .orderBy("is_collect", false).findAll();
            if (list != null) {
                historyAdapter.setData(list);
            }
        } catch (DbException e) {
            e.printStackTrace();
        }
    }

    /**
     * 取消置顶操作
     *
     * @param recordBean
     */
    public void cancelTopping(HistoryRecordBean recordBean) {
        recordBean.is_collect = 1;
        try {
            db.saveOrUpdate(recordBean);
            list = db.selector(HistoryRecordBean.class)
//                    .orderBy("id", true)
                    .orderBy("is_collect", false).findAll();
//            list = db.findAll(HistoryRecordBean.class);
            if (list != null) {
                historyAdapter.setData(list);
            }
        } catch (DbException e) {
            e.printStackTrace();
        }

    }

    /**
     * 删除记录
     *
     * @param recordBean
     */
    public void deleteRecord(HistoryRecordBean recordBean) {
        try {
            db.delete(recordBean);
            list = db.findAll(HistoryRecordBean.class);

            if (list != null) {
                activity.upTabBadge(0, list.size());
            }
            if (list != null) {
                historyAdapter.setData(list);
                if (is_show_delete) {
                    historyAdapter.clearChebox();
                    historyAdapter.initChebox(list.size());
                }
            }
        } catch (DbException e) {
            e.printStackTrace();
        }
    }

    /**
     * 显示清除所有/取消删除
     */
    public void quanXuan(boolean show_delete) {
        is_show_delete = show_delete;
        if (historyAdapter != null) {
            if (is_show_delete) {
                historyAdapter.initChebox(historyAdapter.getItemCount());
            }
            historyAdapter.notifyDataSetChanged();
        }
        if (is_show_delete) {
            llDelete.setVisibility(View.VISIBLE);
            addData.setVisibility(View.GONE);
        } else {
            llDelete.setVisibility(View.GONE);
            addData.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 反选
     */
    public void fanXuan() {
        if (historyAdapter != null) {
            Map<Integer, Boolean> cheboxMap = historyAdapter.getCheboxMap();
            for (int key : cheboxMap.keySet()) {
                cheboxMap.put(key, !cheboxMap.get(key));
            }
            historyAdapter.notifyDataSetChanged();
        }
    }

//    /**
//     * 创建菜单：
//     */
//    SwipeMenuCreator mSwipeMenuCreator = new SwipeMenuCreator() {
//        @Override
//        public void onCreateMenu(SwipeMenu leftMenu, SwipeMenu rightMenu, int viewType) {
//
//            Log.i(TAG, "onCreateMenu: " + viewType);
//            // 在Item右侧添加一个菜单。
//            // 1.编辑
//            // 各种文字和图标属性设置。
//            SwipeMenuItem modifyItem = new SwipeMenuItem(getContext())
//                    .setBackgroundColor(getResources().getColor(R.color.color_line_e1))
//                    .setText("置顶")
//                    .setTextColor(Color.WHITE)
//                    .setTextSize(15) // 文字大小。
//                    .setWidth(180)
//                    .setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
//            rightMenu.addMenuItem(modifyItem);
//            // 2 删除
//            SwipeMenuItem deleteItem = new SwipeMenuItem(getContext());
//            deleteItem.setText("删除")
//                    .setBackgroundColor(getResources().getColor(R.color.color_red))
//                    .setTextColor(Color.WHITE) // 文字颜色。
//                    .setTextSize(15) // 文字大小。
//                    .setWidth(180)
//                    .setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
//
//            rightMenu.addMenuItem(deleteItem);
//
//            // 注意：哪边不想要菜单，那么不要添加即可。
//        }
//
//    };
//
//    /**
//     * 菜单点击监听。
//     */
//    SwipeMenuItemClickListener mMenuItemClickListener = new SwipeMenuItemClickListener() {
//        @Override
//        public void onItemClick(SwipeMenuBridge menuBridge) {//
//            // 任何操作必须先关闭菜单，否则可能出现Item菜单打开状态错乱。
//            menuBridge.closeMenu();
//
////                int direction = menuBridge.getDirection(); // 左侧还是右侧菜单。
//            int adapterPosition = menuBridge.getAdapterPosition(); // RecyclerView的Item的position。
//            int menuPosition = menuBridge.getPosition(); // 菜单在RecyclerView的Item中的Position。
//
//            HistoryRecordBean recordBean = null;
//            if (list != null) {
//                recordBean = list.get(adapterPosition);
//            }
//            if (menuPosition == 0) {//置顶操作
//                if (recordBean != null) {
//                    if (recordBean.is_collect == 0) {
//
//                        cancelTopping(recordBean);
//                        menuBridge.setText("置顶");
//                    } else {
//                        topping(recordBean);
//                        menuBridge.setText("取消置顶");
//                    }
//                }
//
//            } else {//删除操作
//                if (recordBean != null) {
//                    deleteRecord(recordBean);
//                }
//
//            }
//        }
//    };
}
