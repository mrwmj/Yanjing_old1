package com.sdattg.yanjing.bean.zuji;

import org.xutils.db.annotation.Column;
import org.xutils.db.annotation.Table;

/**
 * 收藏
 * Created by Administrator on 2019/7/29.
 */

@Table(name = "ShouCang")
public class ShouCangBean {
    //不可以少
    @Column(name = "id", isId = true)
    public int id;

    @Column(name = "min_name")
    public String min_name;//缩略名

    @Column(name = "time")
    public String time;//时间

    @Column(name = "book_name")
    public String book_name;//书名

    @Column(name = "chapter_name")
    public String chapter_name;//章节

    @Column(name = "is_collect")
    public int is_collect;//是否置顶
}
