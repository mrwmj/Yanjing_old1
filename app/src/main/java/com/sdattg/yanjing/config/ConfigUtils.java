package com.sdattg.yanjing.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;


/**
 * 定义所有访问sharedpreference的方法
 */
public class ConfigUtils {

	// 存放sp信息的文件名
	public static final String CONFIG_NAME = "YanJing";

	private SharedPreferences mSp = null;

	public ConfigUtils(Context context) {
		mSp = context.getSharedPreferences(CONFIG_NAME, 0);
	}

	/**
	 * 清空配置文件里的数据
	 */
	public void clear() {
		mSp.edit().clear().apply();
	}

	/**
	 * 移除某一条数据
	 * @param key 键
     */
	public void remove(String key) {
		mSp.edit().remove(key).apply();
	}

	/**
	 * 添加boolean值
	 * @param key 为String类型
	 * @param value boolean类型
     * @return boolean类型，表示添加成功或失败
     */
	public boolean put(String key, boolean value) {
		return mSp.edit().putBoolean(key, value).commit();
	}

	/**
	 * 添加int值
	 * @param key 为String类型
	 * @param value int类型
     * @return boolean类型，表示添加成功或失败
     */
	public boolean put(String key, int value) {
		return mSp.edit().putInt(key, value).commit();
	}

	/**
	 * 添加long值
	 * @param key 为String类型
	 * @param value long类型
	 * @return boolean类型，表示添加成功或失败
	 */
	public boolean put(String key, long value) {
		return mSp.edit().putLong(key, value).commit();
	}

	/**
	 * 添加float值
	 * @param key 为String类型
	 * @param value float类型
	 * @return boolean类型，表示添加成功或失败
	 */
	public boolean put(String key, float value) {
		return mSp.edit().putFloat(key, value).commit();
	}

	/**
	 * 添加String值
	 * @param key 为String类型
	 * @param value String类型
	 * @return boolean类型，表示添加成功或失败
	 */
	public boolean put(String key, String value) {
		return mSp.edit().putString(key, value).commit();
	}

	/**
	 * 获取boolean值
	 * @param key 为String类型
	 * @param defaultValue 默认值
	 * @return boolean类型
	 */
	public boolean getBoolean(String key, boolean defaultValue) {
		return mSp.getBoolean(key, defaultValue);
	}

	/**
	 * 获取int值
	 * @param key 为String类型
	 * @param defaultValue 默认值
	 * @return int类型
	 */
	public int getInt(String key, int defaultValue) {
		return mSp.getInt(key, defaultValue);
	}

	/**
	 * 获取long值
	 * @param key 为String类型
	 * @param defaultValue 默认值
	 * @return long类型
	 */
	public long getLong(String key, long defaultValue) {
		return mSp.getLong(key, defaultValue);
	}

	/**
	 * 获取float值
	 * @param key 为String类型
	 * @param defaultValue 默认值
	 * @return float类型
	 */
	public float getFloat(String key, float defaultValue) {
		return mSp.getFloat(key, defaultValue);
	}

	/**
	 * 获取String值
	 * @param key 为String类型
	 * @param defaultValue 默认值
	 * @return String类型
	 */
	public String getString(String key, String defaultValue) {
		return mSp.getString(key, defaultValue);
	}

	/**
	 * 返回程序的配置文件的绝对路径 /data/data/packagename/shared_prefs/account
	 * @return
	 */
	private String getSPFilePathNoSuffix(Context context) {
		ApplicationInfo ai = context.getApplicationInfo();
		StringBuilder sb = new StringBuilder().append(ai.dataDir);
		sb.append("/shared_prefs/").append(CONFIG_NAME);
		return sb.toString();
	}
}
