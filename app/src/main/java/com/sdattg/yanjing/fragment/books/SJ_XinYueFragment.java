package com.sdattg.yanjing.fragment.books;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.Utils.ToastUtil;
import com.sdattg.yanjing.adapter.SJXinYueJiuGongGeAdapter;
import com.sdattg.yanjing.adapter.SJXinYueLieBiaoAdapter;
import com.sdattg.yanjing.base.BaseFragment;
import com.sdattg.yanjing.bean.BooksBean;
import com.sdattg.yanjing.bean.CatalogBean;
import com.sdattg.yanjing.common.Constant;
import com.sdattg.yanjing.config.ConfigKey;
import java.util.ArrayList;

import butterknife.Bind;

/**
 * 圣经新约
 * Created by Administrator on 2019/8/12.
 */

@SuppressLint("ValidFragment")
public class SJ_XinYueFragment extends BaseFragment {
    private static String TAG = "SJ_XinYueFragment";
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;


    private ArrayList<BooksBean> beans = new ArrayList<>();
    private SJXinYueLieBiaoAdapter lieBiaoAdapter;
    private SJXinYueJiuGongGeAdapter jiuGongGeAdapter;

    private CatalogBean catalogBean;

    private int page = 1;

    @SuppressLint("ValidFragment")
    public SJ_XinYueFragment(CatalogBean catalogBean) {
        this.catalogBean = catalogBean;
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_sj_xin_yue;
    }

    @Override
    public void initView() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        String string = mConfigUtils.getString(ConfigKey.SHOW_STYLE, Constant.LIE_BIAO);
        if (string.equals(Constant.LIE_BIAO)) {
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext().getApplicationContext()));
            lieBiaoAdapter = new SJXinYueLieBiaoAdapter(getContext());
            recyclerView.setAdapter(lieBiaoAdapter);
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getContext().getApplicationContext(), 3));
            jiuGongGeAdapter = new SJXinYueJiuGongGeAdapter(getContext());
            recyclerView.setAdapter(jiuGongGeAdapter);
        }
    }


    @Override
    public void initListener() {

    }

    @Override
    public void initData() {
        onLoadData();
    }


    private void onLoadData() {
        beans.clear();
        Log.i(TAG, "initData:" + catalogBean.getCatalog_name() + "  上级目录=" + catalogBean.getLast_catalog_id() + "  目录id=" + catalogBean.getCatalog_id());
        Cursor cursor;
        if (catalogBean.getCatalog_id().equals("-1")) {
            cursor = dbManager.queryAll("books", "catalog_id=?", new String[]{catalogBean.getLast_catalog_id()}, "books_min_name asc");
        } else {
            cursor = dbManager.queryAll("books", "catalog_id=? and catalog_id_2=?", new String[]{catalogBean.getLast_catalog_id(), catalogBean.getCatalog_id()}, "books_min_name asc");
        }

        while (cursor.moveToNext()) {
            String books_name = cursor.getString(cursor.getColumnIndex("books_name"));
            String books_min_name = cursor.getString(cursor.getColumnIndex("books_min_name"));
            String books_version = cursor.getString(cursor.getColumnIndex("books_version"));
            String books_summary = cursor.getString(cursor.getColumnIndex("books_summary"));
            String books_id = cursor.getString(cursor.getColumnIndex("books_id"));
            String catalog_rank = cursor.getString(cursor.getColumnIndex("catalog_rank"));
            String catalog_id = cursor.getString(cursor.getColumnIndex("catalog_id"));
            String catalog_id_2 = cursor.getString(cursor.getColumnIndex("catalog_id_2"));

            BooksBean booksBean = new BooksBean();
            booksBean.setBooks_name(books_name);
            booksBean.setBooks_min_name(books_min_name);
            booksBean.setBooks_version(books_version);
            booksBean.setBooks_summary(books_summary);
            booksBean.setBooks_id(books_id);
            booksBean.setCatalog_rank(catalog_rank);
            booksBean.setCatalog_id(catalog_id);
            beans.add(booksBean);
        }
        cursor.close();
        Log.i(TAG, "initData2:" + catalogBean.getCatalog_name() + beans.size());
        String string = mConfigUtils.getString(ConfigKey.SHOW_STYLE, Constant.LIE_BIAO);
        if (string.equals(Constant.LIE_BIAO)) {
            lieBiaoAdapter.setBooks(beans);
        } else {
            jiuGongGeAdapter.setBooks(beans);
        }
    }

    public void onLoadMord() {
        ToastUtil.show(mActivity, "已经到底部了");
    }
}
