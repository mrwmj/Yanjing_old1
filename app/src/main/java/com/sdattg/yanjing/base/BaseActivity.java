package com.sdattg.yanjing.base;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.sdattg.yanjing.R;
import com.sdattg.yanjing.Utils.DbUtils;
import com.sdattg.yanjing.Utils.UIUtils;
import com.sdattg.yanjing.config.ConfigUtils;
import com.sdattg.yanjing.interfaces.UiInterface;

import org.xutils.DbManager;

import java.util.LinkedList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by Administrator on 2019/7/10.
 */

public abstract class BaseActivity extends AppCompatActivity implements UiInterface {

    // 权限数组
    private final static String[] permissions =
            new String[]{
                    "android.permission.WRITE_EXTERNAL_STORAGE",
                    "android.permission.WRITE_CONTACTS",
                    "android.permission.READ_PHONE_STATE",
                    "android.permission.READ_CALENDAR",
                    "android.permission.CAMERA",
                    "android.permission.BODY_SENSORS",
                    "android.permission.ACCESS_COARSE_LOCATION",
                    "android.permission.ACCESS_FINE_LOCATION",
                    "android.permission.INSTALL_PACKAGES",
                    "android.permission.RECORD_AUDIO",
                    "android.permission.SEND_SMS"};


    /**
     * 存放用户打开的activity
     */
    public final static List<BaseActivity> mActivities = new LinkedList<>();
    /**
     * 访问网络的工具类
     */
//    protected NetKit mNetKit;
    /**
     * 简单的用户设置的工具类
     */
    protected ConfigUtils mConfigUtils;
    /**
     * 进度条显示类
     */
//    private ProgressUtils progressUtils;

    private RelativeLayout base_rootView;
    private RelativeLayout rootTitle;
    protected ImageView whiteBack;
    protected ImageView leftImage;
    protected TextView middleTitle;
    protected TextView rightText;
    protected ImageView rightImage;
    protected Context mContext;
    private DbManager db;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 权限检查
        Acp.getInstance(this).request(new AcpOptions.Builder().setPermissions(permissions).build(), new AcpListener() {
            public void onDenied(List<String> paramAnonymousList) {
                UIUtils.showToast("权限拒绝，会导致应用功能，程序退出");
                finish();
            }

            public void onGranted() {
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && useStateBarColor()) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(setStateBarColor()));
        }
//        mNetKit = NetKit.getInstance();
//        progressUtils = new ProgressUtils(this);
        mConfigUtils = new ConfigUtils(this);

        synchronized (mActivities) {
            mActivities.add(this);
        }
        mContext = this;
        setContentView(R.layout.activity_base_layout);
        ButterKnife.bind(this);
        db = DbUtils.getDB();

        initView();
        initListener();
        initData();
    }

    @Override
    public void setContentView(int layoutResID) {
        if (useComTitleLayout()) {//使用统一的title和侧边栏
            super.setContentView(layoutResID);
            FrameLayout linearLayout = (FrameLayout) findViewById(R.id.rootView);
            View view = View.inflate(this, getLayout(), null);
            linearLayout.addView(view);
            superPrepare();
        } else {
            //自定义布局
            super.setContentView(getLayout());
        }
    }

    private void superPrepare() {
        base_rootView = (RelativeLayout) findViewById(R.id.base_rootView);
        rootTitle = (RelativeLayout) findViewById(R.id.title_root_view);
        whiteBack = (ImageView) findViewById(R.id.iv_back_white);
        leftImage = (ImageView) findViewById(R.id.iv_left_icon);
        middleTitle = (TextView) findViewById(R.id.title_name);
        rightText = (TextView) findViewById(R.id.base_rigth_text);
        rightImage = (ImageView) findViewById(R.id.iv_right_icon);
        whiteBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    /**
     * 设置中间标题
     *
     * @param title
     */
    public void setMiddleTitle(String title) {
        if (middleTitle != null) {
            middleTitle.setText(title);
        }
    }

    /**
     * 设置中间标题颜色
     */
    private void setTitleColor() {
        if (middleTitle != null) {
            middleTitle.setTextColor(Color.WHITE);
        }
    }


    /**
     * 设置顶部标题隐藏
     *
     */
    public void setActionBarGone() {
        if (rootTitle != null) {
            rootTitle.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        synchronized (mActivities) {
            mActivities.remove(this);
        }
    }

    /**
     * 是否使用app统一的Title layout，如果需要自定义，复写该函数
     *
     * @return 默认使用true
     */
    protected boolean useComTitleLayout() {
        return true;
    }


    /**
     * 设置状态栏颜色
     *
     * @return
     */
    protected boolean useStateBarColor() {
        return false;
    }


    /**
     * 改变状态栏颜色
     */
    private int setStateBarColor() {
        int res = R.color.colorLightRed;
        if (-1 != getStateBarColor()) {
            res = getStateBarColor();
        }
        return res;
    }

    /**
     * 状态栏统一使用自定义的颜色，若要修改颜色，复写getStateBarColor()
     *
     * @return 颜色值
     */
    protected int getStateBarColor() {
        return -1;
    }
    /**
     * 改变标题栏颜色
     *
     * @param colorId
     */
    private void setTitleBgColor(int colorId) {
        if (rootTitle != null) {
            rootTitle.setBackgroundResource(colorId);
        }
    }
}
