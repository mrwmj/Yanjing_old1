package com.sdattg.yanjing.activity;

import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.Utils.DbUtils;
import com.sdattg.yanjing.adapter.SimpleFragmentPagerAdapter;
import com.sdattg.yanjing.bean.zuji.HistoryRecordBean;
import com.sdattg.yanjing.bean.zuji.ShouCangBean;
import com.sdattg.yanjing.bean.zuji.ShuQianBean;
import com.sdattg.yanjing.bean.zuji.TabTitlesBean;
import com.sdattg.yanjing.fragment.zuji.BookmarkFragment;
import com.sdattg.yanjing.fragment.zuji.CollectFragment;
import com.sdattg.yanjing.fragment.zuji.HistoryRecordFragment;
import com.sdattg.yanjing.view.NoScrollViewPager;

import org.xutils.DbManager;
import org.xutils.ex.DbException;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 足迹
 * Created by Administrator on 2019/7/29.
 */

public class FootprintActivity extends AppCompatActivity {
    private static String TAG = "FootprintActivity";
    @Bind(R.id.tab_layout)
    TabLayout tabLayout;
    @Bind(R.id.view_pager)
    NoScrollViewPager viewPager;
    @Bind(R.id.view_close_menu)
    View closeMenu;
    @Bind(R.id.tv_clear_all)
    TextView clearAll;
    @Bind(R.id.root_layout)
    LinearLayout rootLayout;
    @Bind(R.id.iv_back)
    ImageView ivBack;

    private ArrayList<TabTitlesBean> titles = new ArrayList<>();
    private ArrayList<Fragment> fragments = new ArrayList<>();

    private SimpleFragmentPagerAdapter pagerAdapter;
    private DbManager db;


    private List<HistoryRecordBean> hrb = null;
    private List<ShouCangBean> scb = null;
    private List<ShuQianBean> sqb = null;

    private int selectPosition = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foot_print);

        //设置动画
        overridePendingTransition(R.anim.xt_activity_open, R.anim.xt_activity_close_right);

        ButterKnife.bind(this);

        db = DbUtils.getDB();
        initView();
        initListener();
        initData();

    }

    public void initView() {
        fragments.add(new HistoryRecordFragment());
        fragments.add(new CollectFragment());
        fragments.add(new BookmarkFragment());
        try {
            TabTitlesBean titlesBean = new TabTitlesBean();
            titlesBean.title_name = "历史记录";
            hrb = db.findAll(HistoryRecordBean.class);
            if (hrb != null) {
                titlesBean.count = hrb.size();
            } else {
                titlesBean.count = 0;
            }

            TabTitlesBean titlesBean2 = new TabTitlesBean();
            titlesBean2.title_name = "收藏书籍";
            scb = db.findAll(ShouCangBean.class);
            if (scb != null) {
                titlesBean2.count = scb.size();
            } else {
                titlesBean2.count = 0;
            }

            TabTitlesBean titlesBean3 = new TabTitlesBean();
            titlesBean3.title_name = "我的书签";
            sqb = db.findAll(ShuQianBean.class);
            if (sqb != null) {
                titlesBean3.count = sqb.size();
            } else {
                titlesBean3.count = 0;
            }
            titles.add(titlesBean);
            titles.add(titlesBean2);
            titles.add(titlesBean3);
        } catch (DbException e) {
            e.printStackTrace();
        }


        for (int i = 0; i < titles.size(); i++) {
            tabLayout.addTab(tabLayout.newTab().setText(titles.get(i).title_name));
        }

        tabLayout.setupWithViewPager(viewPager);
        pagerAdapter = new SimpleFragmentPagerAdapter(this, getSupportFragmentManager(), fragments, titles);
        viewPager.setAdapter(pagerAdapter);

    }

    public void initListener() {

        clearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (selectPosition) {
                    case 0://历史记录
                        clearAll.setText("反选");
                        HistoryRecordFragment hFragment = (HistoryRecordFragment) fragments.get(selectPosition);
                        if (hFragment.is_show_delete) {
                            hFragment.fanXuan();
                        } else {
                            hFragment.quanXuan(true);
                        }
                        break;
                    case 1://收藏书籍
                        CollectFragment cFragment = (CollectFragment) fragments.get(selectPosition);
                        clearAll.setText("反选");
                        if (cFragment.is_show_delete) {
                            cFragment.fanXuan();
                        } else {
                            cFragment.quanXuan(true);
                        }
                        break;
                    case 2://我的书签
                        clearAll.setText("反选");
                        BookmarkFragment bFragment = (BookmarkFragment) fragments.get(selectPosition);
                        if (bFragment.is_show_delete) {
                            bFragment.fanXuan();
                        } else {
                            bFragment.quanXuan(true);
//                            bFragment.setShowDelete(true);
                        }
                        break;
                }
            }
        });


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectPosition = tab.getPosition();
                switch (selectPosition) {
                    case 0://历史记录
                        HistoryRecordFragment hFragment = (HistoryRecordFragment) fragments.get(selectPosition);
                        if (hFragment.is_show_delete) {
                            clearAll.setText("反选");
                        } else {
                            clearAll.setText("清空所有");
                        }
                        break;
                    case 1://收藏书籍
                        CollectFragment cFragment = (CollectFragment) fragments.get(selectPosition);
                        if (cFragment.is_show_delete) {
                            clearAll.setText("反选");
                        } else {
                            clearAll.setText("清空所有");
                        }
                        break;
                    case 2://我的书签
                        BookmarkFragment bFragment = (BookmarkFragment) fragments.get(selectPosition);
                        if (bFragment.is_show_delete) {
                            clearAll.setText("反选");
                        } else {
                            clearAll.setText("清空所有");
                        }
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        closeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void initData() {
        setUpTabBadge();
    }


    /**
     * 设置Tablayout上的标题的角标
     */
    private void setUpTabBadge() {
        // 1. 最简单
//        for (int i = 0; i < fragments.size(); i++) {
//            mBadgeViews.get(i).setTargetView(((ViewGroup) tabLayout.getChildAt(0)).getChildAt(i));
//            mBadgeViews.get(i).setText(formatBadgeNumber(mBadgeCountList.get(i)));
//        }

        // 2. 最实用
        for (int i = 0; i < fragments.size(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);

            // 更新Badge前,先remove原来的customView,否则Badge无法更新
            View customView = tab.getCustomView();
            if (customView != null) {
                ViewParent parent = customView.getParent();
                if (parent != null) {
                    ((ViewGroup) parent).removeView(customView);
                }
            }
            // 更新CustomView
            tab.setCustomView(pagerAdapter.getTabItemView(i));
        }
        // 需加上以下代码,不然会出现更新Tab角标后,选中的Tab字体颜色不是选中状态的颜色
        tabLayout.getTabAt(tabLayout.getSelectedTabPosition()).getCustomView().setSelected(true);
    }


    @Override
    protected void onResume() {
        super.onResume();

        //启动时动画加载完成后，设置阴影层
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(320);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            rootLayout.setBackgroundResource(R.color.color_bg_open);
                        }
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    @Override
    public void finish() {
        super.finish();
        rootLayout.setBackgroundResource(R.color.color_bg_open2);//设置退场动画时阴影消失
        overridePendingTransition(0, R.anim.xt_activity_close);
    }

    public void upTabBadge(int type, int count) {
        Log.i(TAG, "upTabBadge: type=" + type + "  count=" + count);

        TabLayout.Tab tab = tabLayout.getTabAt(type);
        // 更新Badge前,先remove原来的customView,否则Badge无法更新
        View customView = tab.getCustomView();
        if (customView != null) {
            ViewParent parent = customView.getParent();
            if (parent != null) {
                ((ViewGroup) parent).removeView(customView);
            }
        }
        titles.get(type).count = count;
        // 更新CustomView
        tab.setCustomView(pagerAdapter.getTabItemView(type));
        // 需加上以下代码,不然会出现更新Tab角标后,选中的Tab字体颜色不是选中状态的颜色
        tabLayout.getTabAt(tabLayout.getSelectedTabPosition()).getCustomView().setSelected(true);
    }


    public void setClearAllText(String str) {
        clearAll.setText(str);
    }

}
