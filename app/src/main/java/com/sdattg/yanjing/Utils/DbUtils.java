package com.sdattg.yanjing.Utils;

import android.util.Log;

import org.xutils.DbManager;
import org.xutils.DbManager.DaoConfig;
import org.xutils.DbManager.DbOpenListener;
import org.xutils.DbManager.DbUpgradeListener;
import org.xutils.db.table.TableEntity;
import org.xutils.x;

/**
 * 数据库配置类
 */
public class DbUtils {
    //初始化数据库的 配置文件
    private static DaoConfig daoConfig;
    //数据库名称
    private static String DB_NAME = "yanjing2.db";
    public static DaoConfig getDaoConfig() {
        if (daoConfig == null) {
            daoConfig = new DaoConfig()
                    .setDbName(DB_NAME)
                    .setDbDir(FileUtils.getDir_de1())
                    .setDbVersion(1)
                    .setAllowTransaction(true)
                    .setDbOpenListener(new DbOpenListener() {
                public void onDbOpened(DbManager db) {
                    db.getDatabase().enableWriteAheadLogging();
                }
            }).setDbUpgradeListener(new DbUpgradeListener() {
                //在数据库进行更新之后，需要进行处理的操作
                public void onUpgrade(DbManager db, int oldVersion, int newVersion) {
                }
            }).setTableCreateListener(new DbManager.TableCreateListener() {
                        @Override
                        public void onTableCreated(DbManager dbManager, TableEntity<?> tableEntity) {
                            Log.i("JAVA", "onTableCreated：" + tableEntity.getName());
                        }
                    });
        }
        return daoConfig;
    }
    //获取DB提供的数据库的配置文件
    public static DbManager getDB() {
        return x.getDb(getDaoConfig());
    }
}
