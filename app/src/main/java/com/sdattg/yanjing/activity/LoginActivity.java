package com.sdattg.yanjing.activity;


import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.Utils.UIUtils;
import com.sdattg.yanjing.base.BaseActivity;
import com.sdattg.yanjing.config.ConfigKey;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 登录
 * Created by Administrator on 2019/7/10.
 */

public class LoginActivity extends BaseActivity {
    private static String TAG = "LoginActivity";
    @Bind(R.id.et_login_account)
    EditText etLoginAccount;
    @Bind(R.id.line_login_phone)
    View lineLoginPhone;
    @Bind(R.id.et_login_password)
    EditText etLoginPassword;
    @Bind(R.id.line_login_password)
    View lineLoginPassword;
    @Bind(R.id.tv_login)
    TextView tvLogin;
    @Bind(R.id.tv_register)
    TextView tvRegister;
    @Bind(R.id.tv_forget_password)
    TextView tvForgetPassword;


    @Override
    public int getLayout() {
        return R.layout.activity_login;
    }


    @Override
    public void initView() {
        setActionBarGone();

        String account = mConfigUtils.getString(ConfigKey.LOGIN_ACCOUNT, "");
        String password = mConfigUtils.getString(ConfigKey.PASSWORD, "");
        if (account != null) {
            etLoginAccount.setText(account);
        }
        if (password != null) {
            etLoginPassword.setText(password);
        }
    }

    @Override
    public void initListener() {
        /**
         *  获取到焦点的监听
         */
        etLoginAccount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    lineLoginPhone.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                } else {
                    lineLoginPhone.setBackgroundColor(getResources().getColor(R.color.color_text_cf));
                }
            }
        });

        etLoginPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    lineLoginPassword.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                } else {
                    lineLoginPassword.setBackgroundColor(getResources().getColor(R.color.color_text_cf));
                }
            }
        });

    }

    @Override
    public void initData() {

    }

    @OnClick({R.id.tv_login, R.id.tv_forget_password,R.id.tv_register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_login://登录
                login();
                break;
            case R.id.tv_forget_password://忘记密码
                forgetPassword();
                break;
            case R.id.tv_register:
                startActivityForResult(new Intent(LoginActivity.this, RegisterActivity.class), 101);
                break;
        }
    }


    private void forgetPassword() {
        startActivity(new Intent(LoginActivity.this, ForgetPasswordActivity.class));
    }

    private void login() {
        String account = etLoginAccount.getText().toString().trim();
        String password = etLoginPassword.getText().toString().trim();

        if (account == null || account.equals("")) {
            UIUtils.showToast("请输入账号");
            return;
        }
        if (password == null || password.equals("")) {
            UIUtils.showToast("请输入密码");
            return;
        }

//        UIUtils.checkMobile()

        mConfigUtils.put(ConfigKey.LOGIN_ACCOUNT, account);
        mConfigUtils.put(ConfigKey.PASSWORD, password);

        UIUtils.showToast("登录成功");

        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 102) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }
    }
}
