package com.sdattg.yanjing.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Administrator on 2017/9/29.
 */

public class DBManager {
    DBHelper dbHelper;

    public DBManager(Context context) {
        dbHelper = DBHelper.getDBHelper(context,1);
    }

    public long insert(String table, ContentValues values) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long insert = db.insert(table, null, values);
        db.close();
        /**
         * 如果返回值为-1，表示插入失败
         */
        return insert;
    }

    /*
     * 修改
     *
     * @param table
     * @param valuses
     * @param whereClause
     * @param whereArgs
     */
    public int upData(String table, ContentValues valuses, String whereClause, String[] whereArgs) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        int update = db.update(table, valuses, whereClause, whereArgs);
        db.close();
        return update;
    }

    public Cursor query(String table, String selection, String[] selectionArgs, String orderBy) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(table, null, selection, selectionArgs, null, null, orderBy);
        return cursor;
    }

    public Cursor queryAll(String table, String selection, String[] selectionArgs, String orderBy) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(table, null, selection, selectionArgs, null, null, orderBy);
        return cursor;
    }

    //删除整张表
    public void cleaer(String table) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.delete(table, null, null);
        db.close();
    }
}
