package com.sdattg.yanjing.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.sdattg.yanjing.R;
import com.sdattg.yanjing.adapter.WindomSearchAdapter;
import com.sdattg.yanjing.base.BaseActivity;
import com.sdattg.yanjing.bean.SearchBen;
import com.sdattg.yanjing.config.ConfigKey;
import com.sdattg.yanjing.db.DBManager;
import com.sdattg.yanjing.fragment.BaiKeFragment;
import com.sdattg.yanjing.fragment.BenDiFragment;
import com.sdattg.yanjing.fragment.LingXiuFragment;
import com.sdattg.yanjing.fragment.ShuKuFragment;
import com.sdattg.yanjing.interfaces.OnSearchItemClickLister;
import com.sdattg.yanjing.view.titleview.PagerSlidingTabStrip;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * 书库
 * Created by Administrator on 2019/7/24.
 */

public class BookActivity extends BaseActivity {

    private static String TAG = "MainActivity";
    @Bind(R.id.iv_zu_ji)
    ImageView ivRightIcon;//足迹
    @Bind(R.id.search_layout)
    LinearLayout search_layout;
    @Bind(R.id.ll_search)
    LinearLayout ll_search;//选择从那里面搜索
    @Bind(R.id.tv_search_type)
    TextView tv_search_type;//选中搜索
    @Bind(R.id.iv_sx)
    ImageView ivSx;
    @Bind(R.id.et_search)
    EditText et_search;//搜索内容
    @Bind(R.id.tv_search)
    TextView tvSearch;//开始搜索
    @Bind(R.id.tab_layout)
    PagerSlidingTabStrip tabLayout;
    @Bind(R.id.view_pager)
    ViewPager viewPager;


    private DBManager dbManager;
    private Context mContext;
    private ArrayList<String> titles = new ArrayList<>();
    private ArrayList<Fragment> fragments = new ArrayList<>();
    private boolean isShowPopwind = false;
    private PopupWindow popWnd;
    private WindomSearchAdapter adapter;

    @Override
    public int getLayout() {
        return R.layout.activity_book;
    }

    @Override
    public void initView() {
        mContext = BookActivity.this;
        setActionBarGone();//隐藏顶部标题栏
        mConfigUtils.put(ConfigKey.SEARCH_MODE, 4);

        if (this.isFinishing()) {
            return;
        }

    }

    @Override
    public void initListener() {

        /**
         * 头部左侧按钮
         */
        ivRightIcon.setOnClickListener(mOnClickListener);

        /**
         * 搜索
         */
        tvSearch.setOnClickListener(mOnClickListener);

        ll_search.setOnClickListener(mOnClickListener);
    }

    @Override
    public void initData() {
        titles.add("书库");
        titles.add("灵修");
        titles.add("百科");
        titles.add("本地");

        fragments.add(new ShuKuFragment());
        fragments.add(new LingXiuFragment());
        fragments.add(new BaiKeFragment());
        fragments.add(new BenDiFragment());
        BooksPager adapter = new BooksPager(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setViewPager(viewPager);
    }


    /**
     * 显示搜索选项的popwidn
     */
    private void showPopWind() {
        View contentView = LayoutInflater.from(BookActivity.this)
                .inflate(R.layout.activity_windom_search, null);
        RecyclerView recyclerView = contentView.findViewById(R.id.recycler_view);//历史账号记录
        setDataList(recyclerView);
        LinearLayout parent = (LinearLayout) ll_search.getParent();
        popWnd = new PopupWindow(BookActivity.this);
        popWnd.setContentView(contentView);
        popWnd.setWidth(parent.getWidth());
        popWnd.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);

        popWnd.setOutsideTouchable(false);
        popWnd.setBackgroundDrawable(new BitmapDrawable());

        //相对某个控件的位置，有偏移;
        // xoff表示x轴的偏移，正值表示向左，负值表示向右；
        // yoff表示相对y轴的偏移，正值是向下，负值是向上；
        popWnd.showAsDropDown(ll_search, 0, 0);

        popWnd.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
//                isShowPopwind = false;
            }
        });
    }


    /**
     * 设置popwind下拉显示的内容
     */
    private void setDataList(final RecyclerView recyclerView) {
        final ArrayList<SearchBen> searchBens = new ArrayList<>();
        final SearchBen searchBen = new SearchBen();
        searchBen.setId(1);
        searchBen.setName("搜书籍");

        SearchBen searchBen2 = new SearchBen();
        searchBen2.setId(2);
        searchBen2.setName("搜目录");

        SearchBen searchBen3 = new SearchBen();
        searchBen3.setId(3);
        searchBen3.setName("搜标题");

        SearchBen searchBen4 = new SearchBen();
        searchBen4.setId(4);
        searchBen4.setName("搜内容");

        searchBens.add(searchBen);
        searchBens.add(searchBen2);
        searchBens.add(searchBen3);
        searchBens.add(searchBen4);

        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter = new WindomSearchAdapter(searchBens, BookActivity.this, new OnSearchItemClickLister() {
            @Override
            public void onItemClick(SearchBen con) {
                tv_search_type.setText(con.getName());
                isShowPopwind = false;
                if (popWnd != null) {
                    popWnd.dismiss();
                }
            }
        });
        recyclerView.setAdapter(adapter);
    }


    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.iv_zu_ji://顶部左侧  足迹

                    Intent intent = new Intent(mContext, FootprintActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
//                    drawerLayout.openDrawer(Gravity.LEFT);
                    break;
                case R.id.tv_search://搜索
                    startActivity(new Intent(mContext, SearchActivity.class));
                    break;
                case R.id.ll_search://
                    if (isShowPopwind) {
                        ivSx.setImageResource(R.drawable.you);
                        isShowPopwind = false;
                        if (popWnd != null) {
                            popWnd.dismiss();
                        }
                    } else {
                        ivSx.setImageResource(R.drawable.xia);
                        isShowPopwind = true;
                        showPopWind();
                    }
                    break;
            }

        }
    };

    private class BooksPager extends FragmentPagerAdapter {

        private BooksPager(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles.get(position);
        }

        @Override
        public int getCount() {
            return titles.size();
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return fragments.get(0);
                case 1:
                    return fragments.get(1);
                case 2:
                    return fragments.get(2);
                case 3:
                    return fragments.get(3);
            }
            return null;
        }
    }
}
