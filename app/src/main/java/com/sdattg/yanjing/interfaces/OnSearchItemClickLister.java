package com.sdattg.yanjing.interfaces;

import com.sdattg.yanjing.bean.SearchBen;

/**
 * Created by Administrator on 2018/1/19.
 */

public interface OnSearchItemClickLister {
    void onItemClick(SearchBen con);
}
